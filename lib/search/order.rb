# frozen_string_literal: true

class Search::Order < Administrate::Order
  def apply(relation)
    return relation unless %i[desc asc].include? direction.to_sym
    return relation if attribute.nil?

    model = relation.model
    if model.respond_to?(:arel_column_node)
      # This is for Entry::Payload
      column = model.arel_column_node(attribute)
    else
      colum = model.arel_table[attribute.to_sym]
    end

    if column
      if column.respond_to? direction
        relation.order(column.public_send(direction))
      else
        relation.order(column => direction)
      end
    else
      relation
    end
  end
end
