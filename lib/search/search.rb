# frozen_string_literal: true

require 'search/terms'
require 'search/order'
module Search
  class UnknownFilterFieldException < RuntimeError; end
  class UnknownScopeException < RuntimeError; end

  class Search
    def initialize(resource_class, terms)
      @resource_class = resource_class
      @terms = Terms.parse(terms)
    end

    attr_reader :resource_class, :terms
    attr_accessor :query

    def results
      @results ||= run
    end

    def run
      @query = base_query

      terms.fields.each do |field|
        filter_method = "by_#{field}".to_sym
        next unless @query.respond_to? filter_method
        @query = @query.public_send(filter_method, terms[field])
        # else
      end

      terms.scopes.each do |scope|
        @query = @query.public_send scope if @query.respond_to?(scope)
      end

      @query = @query.by_any(terms.any) if terms.any?

      @query
    end

    def base_query
      if terms.scopes.include? :deleted
        resource_class.unscoped
      else
        resource_class.all
      end
    end
  end
end
