# frozen_string_literal: true

module Search
  class Terms
    def self.parse(term)
      terms = new
      term.scan(/((?:(\w[\w\-_]+):)?(?:([\w\-_\*]+)|\"([^\"]+)\"))/).each do |match|
        terms.add match[1], match[2].nil? ? match[3] : match[2]
      end
      terms
    end

    def add(field, term)
      case field
      when 'is'
        scopes << term.to_sym
      when nil
        any << term
      else
        self[field] << term
      end
    end

    def [](field)
      storage[field.to_sym] ||= []
    end

    def fields
      storage.keys
    end

    def scopes
      @scopes ||= []
    end

    def any
      @any ||= []
    end

    delegate :any?, to: :any

    private

    def storage
      @storage ||= {}
    end
  end
end
