# frozen_string_literal: true

class Service::Base
  attr_reader :error
  attr_reader :flash

  def initialize
    @error = nil

    @flash = {}
  end

  def success?
    @error.nil?
  end

  def call
    raise NotImplementedError
  end

  def handle_error(error)
    @error = error
  end

  def fail!(error)
    handle_error(error)

    self
  end

  class << self
    def inherited(subclass)
      subclass.const_set :Error, Class.new(StandardError)
    end

    def call(*arguments)
      new(*arguments).call
    end
  end
end
