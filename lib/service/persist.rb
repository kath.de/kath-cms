# frozen_string_literal: true

class Service::Persist < Service::Resource
  attr_reader :params

  def initialize(resource, params = {})
    super(resource)

    # instantiate resource if this is a create operation
    @resource = resource.new if @resource.is_a? Class
    @params = params

    @prepared = false
    @saved = false
  end

  def call
    prepare

    @saved = do_save? && !save_resource.nil?

    if @saved
      after_save
    else
      @error = 'Resource could not be saved'
    end

    self
  end

  def prepare
    # do not run twice
    unless @prepared
      @data = filter_params(@params)

      prepare_data
      assign_attributes
      update_resource

      @prepared = true
    end

    self
  end

  def saved?
    @saved
  end

  protected

  attr_reader :data

  def prepare_data; end

  def update_resource; end

  def after_save; end

  def assign_attributes
    resource.assign_attributes(data)
  end

  def filter_params(params)
    params.to_h.symbolize_keys
  end

  def save_resource
    resource.save
  end

  def do_save?
    resource.valid?
  end
end
