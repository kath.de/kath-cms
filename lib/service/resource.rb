# frozen_string_literal: true

class Service::Resource < Service::Base
  attr_reader :resource

  def initialize(resource)
    super()

    @resource = resource
  end
end
