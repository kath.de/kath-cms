# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class ImprovedMetaInspector < SimpleDelegator
  def initialize(url)
    @metainspector = MetaInspector.new(url)
    super(@metainspector)
  end

  def first_sentence_of_description
    return nil if description.blank?

    description.split(/(?<=\S\.)\s/).first
  end

  def descriptions
    [
      meta_tag_values('dc.description'),
      meta_tag_values('description'),
    ].flatten.compact
  end

  def meta_tag_values(key)
    key = key.downcase
    mt = meta_tags
    mt.delete 'charset' # TODO: Somehow this fails if charset is included
    mt.values.map { |h| h.fetch(key, []) }.flatten.compact
  end

  def titles
    [
      metadata_titles,
      title,
      body_title,
      headlines,
    ].flatten
  end

  def metadata_titles
    [
      meta_tag_values('og:title'),
      meta_tag_values('dc.title'),
    ].flatten.compact
  end

  def longest_title
    @metainspector.best_title
  end

  def best_title
    titles.first
  end

  def best_title_stripped
    strip_additional_information(best_title)
  end

  MINIMUM_REAL_TITLE_CHARACTERS = 10
  COMMON_TITLE_SEPARATORS = %r{[-\|/]+}

  def strip_additional_information(title)
    return nil if title.blank?

    parts = title.split(/\s+#{COMMON_TITLE_SEPARATORS}\s/)
    parts.detect { |match| match.length >= MINIMUM_REAL_TITLE_CHARACTERS } || title
  end

  def body_title
    parsed.css('body title')
  end

  def headlines
    [
      parsed.css('h1'),
      parsed.css('h2'),
      parsed.css('h3'),
    ].flatten.compact
  end

  def publication_dates
    normalize_values(
      meta_tag_values('publication_date'),
      meta_tag_values('article:published_time'),
      meta_tag_values('dc.date'),
      meta_tag_values('date'),
      # response.headers['date']
    ).map { |string| Time.zone.parse(string) rescue nil }.compact
  end

  def publication_date
    publication_dates.first
  end

  def update_dates
    [
      meta_tag_values('article:modified_time'),
    ]
  end

  def article_images
    [
      meta_tag_values('og:image'),
      meta_tag_values('urlPhoto'),
    ].flatten.compact
  end

  def canonical_urls
    [
      meta_tag_values('canonical'),
      meta_tag_values('og:url'),
      meta_tag_values('twitter:url'),
      url,
    ].flatten.compact
  end

  def canonical_url
    canonical_urls.first
  end

  def provider_names
    [
      meta_tag_values('og:site_name'),
      meta_tag_values('application-name'),
      meta_tag_values('al:ios:app_name'),
      meta_tag_values('apple-mobile-web-app-title'),
      meta_tag_values('twitter:app:name:googleplay'),
      meta_tag_values('twitter:app:name:ipad'),
      meta_tag_values('twitter:app:name:iphone'),
    ].flatten.compact
  end

  def provider_urls
    [
      meta_tag_values('publisher'),
      meta_tag_values('article:publisher'),
      meta_tag_values('dc.publisher'),
    ].flatten.compact
  end

  def authors
    [
      meta_tag_values('author'),
      meta_tag_values('article:author'),
      meta_tag_values('dc.creator'),
    ].flatten.compact
  end

  def keywords
    [
      meta_tag_values('keywords'),
      meta_tag_values('news_keywords'),
      meta_tag_values('article:tag'),
    ].flatten.compact.map do |keywords|
      if keywords.respond_to? :split
        keywords.split(',').map(&:strip)
      else
        keywords
      end
    end.flatten.compact
  end

  def page_type
    [
      meta_tag_values('og:type'),
      meta_tag_values('dc.type'),
    ].flatten.compact
  end

  def section
    # themengebiet
    [
      meta_tag_values('article:section'),
      meta_tag_values('dc.subject'),
    ].flatten.compact
  end

  private

  def normalize_values(*values)
    values.flatten.compact.map(&:strip)
  end

  module Location
    class Base
      def raw_value
        raise MethodNotImplementedException
      end
    end

    class Element < Base
      attr_reader :node, :attribute_key

      def initialize(node)
        @node = node
      end
    end

    class Meta < Element
      attr_reader :attribute_key

      def initialize(node, attribute_key)
        super(node)
        @attribute_key = attribute_key
      end

      attr_reader :attribute_key

      def attribute_value
        node.attributes[attribute_key]
      end
    end
  end

  module Value
    class Base
      attr_reader :string_value, :location

      def initialize(value, location)
        @string_value = value
        @location = location
      end

      def html_value
        location.raw_value
      end

      def value
        value
      end

      def to_s
        string_value
      end
    end

    class String < Base
      def value
        string_value.strip
      end
    end

    class URL < Base
      def value
        @url ||= cast value
      end

      def cast(string)
        URL.new string
      end
    end
  end
end
