module Administrate::ActiveStorageFieldBackend
  include ThumborRails::Helpers

  def variant(attachment, options)
    case Rails.configuration.image_resize_backend
    when :thumbor
      width = options[:resize_to_limit][0]
      height = options[:resize_to_limit][1]

      blob_url = Rails.application.routes.url_helpers.rails_blob_url(attachment)
      thumbor_url(blob_url, width: width, height: height)
    else
      Rails.application.routes.url_helpers.rails_representation_path(attachment.variant(options), only_path: true)
    end
  end
end
