# frozen_string_literal: true

module Administrate
  module FieldWithResource
    # def initialize(attribute, data, page, resource, options = {})
    #  super
    #  @resource = resource
    # end

    attr_accessor :resource
  end
end
