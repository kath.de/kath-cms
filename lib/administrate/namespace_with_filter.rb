# frozen_string_literal: true

module Administrate
  module NamespaceWithFilter
    INCLUDED_RESSOURCES = (Entry.subclasses << Image << Entry).map{|klass| klass.model_name.collection }

    def resources
      super.select do |resource|
        INCLUDED_RESSOURCES.include? resource.resource
      end
    end
  end
end
