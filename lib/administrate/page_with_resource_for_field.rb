# frozen_string_literal: true

module Administrate
  module PageWithResourceForField
    extend ActiveSupport::Concern

    included do
      public :dashboard
    end

    protected

    def attribute_field(dashboard, resource, attribute_name, page)
      super.tap do |field|
        field.resource = resource
      end
    end
  end
end
