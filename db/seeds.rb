# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command
# (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'yaml'
require 'logger'
require 'tempfile'

Account.create [
  { username: 'johannes.mueller@kath.de', settings: {} },
  { username: 'alexander.radej@kath.de', settings: {} },
  { username: 'maximilian.roell@kath.de', settings: {} },
]

PaperTrail.request.whodunnit = Account.first
PaperTrail.enabled = false

Publisher.create([{ slug: 'kath.de', title: 'kath.de' },
                  { slug: 'de.radiovaticana.va', title: 'Radio Vatikan' },
                  { slug: 'explizit.net', title: 'explizit.net' },
                  { slug: 'br.de', title: 'Bayerischer Rundfunk' },
                  { slug: 'welt.de', title: 'Die Welt' },
                  { slug: 'fr-online.de', title: 'Frankfurter Rundschau' },
                  { slug: 'derwesten.de', title: 'Der Westen' },
                  { slug: 'drs.de', title: 'Diözese Rottenburg-Stuttgart' },
                  { slug: 'bistumlimburg.de', title: 'Bistum Limburg' },
                  { slug: 'erzbischof.kirche-bamberg.de', title: 'Erzbistum Bamberg' },
                  { slug: 'bonifatiuswerk.de', title: 'Bonifatiuswerk' },
                  { slug: 'bistum-hildesheim.de', title: 'Bistum Hildesheim' },]).map(&:publish!)

class Seeder
  def initialize
    logger.info "Rails seeds for env #{Rails.env}"
  end

  def create_settings
    Dir[File.join('db', 'seeds', 'settings', '*.yml')].each do |file|
      puts "loading settings #{File.basename(file, '.yml')} from #{file}"
      update_entry(Settings, File.basename(file, '.yml'), payload: YAML.load_file(file), status: :draft)
    end
  end

  def logger
    @logger ||= Logger.new(STDOUT)
  end

  def read_frontmatter(file)
    content = File.read(file)
    md = content.match(/^(?<metadata>---\s*\n.*?\n?)^(---\s*$\n?)/m)

    abort("file did not contain metadata") unless md

    data = YAML.safe_load(md[:metadata], [Date]).symbolize_keys
    data[:slug] = File.basename(file).gsub(/\.[^\.]+\Z/, '')
    [data, md.post_match]
  end

  def insert_or_update_entries(klass)
    dir = File.join('db', 'seeds', klass.model_name.plural)

    Dir[File.join(dir, '**/*.md')].map do |file|
      begin
        data, body = read_frontmatter(file)
        data[:body] = body
        data[:slug] = file[(dir.size + 1)..-1].gsub(/\.[^\.]+\Z/, '')
        data[:author] = find_or_create_author(data[:author]) if data[:author]

        yield(data) if block_given?

        entry = update_entry(klass, data[:slug], data)

        entry
      rescue StandardError => err
        logger.fatal "Error in #{file}"
        logger.fatal(err)
        throw err
      end
    end
  end

  def update_entry(klass, slug, data)
    slug = slug.parameterize
    entry = klass.by_slug(slug).first_or_initialize(slug: slug)

    original_entry = entry.dup if entry.persisted?
    entry.assign_attributes(data)
    entry.publish unless data.key? :status

    logger.info "Loaded #{klass.name} \e[36m#{entry.slug}\e[0m"

    if entry.persisted?
      if original_entry
        show_diff original_entry.attributes, entry.attributes

        show_diff original_entry.settings, entry.settings if entry.respond_to? :settings
      end
      entry.save
    else
      entry.save
      logger.info 'Created new'
    end
  end

  def show_diff(original, modified)
    keys = diff_keys(original, modified)
    symbols = {
      changed: '±',
      removed: '-',
      added: '+',
    }

    keys.each do |key, op|
      next if key == 'id'
      next if key == 'created_at'
      next if key == 'updated_at'
      next if key == 'payload'

      print symbols[op]
      print ' '
      print key
      puts

      if op == :changed
        a = original[key]
        b = modified[key]
        if a.is_a?(String) && b.is_a?(String) && a.size + b.size > 80
          a_file.rewind
          b_file.rewind
          a_file.write(a)
          b_file.write(b)
          print `git diff --no-index --no-prefix --color #{a_file.path} #{b_file.path}`
          next
        end
      end

      if op == :changed || op == :removed
        print "\e[31m"
        print original[key]
        print "\e[0m"
        puts
      end
      next unless op == :changed || op == :added
      print "\e[32m"
      print modified[key]
      print "\e[0m"
      puts
    end
  end

  def finalize
    @a_file&.close
    @b_file&.close
  end

  def diff_keys(original, modified)
    keys_in_common  = original.keys & modified.keys
    keys_removed    = original.keys - modified.keys
    keys_added      = modified.keys - original.keys

    keys_changed = keys_in_common.reject { |key| original[key] == modified[key] }

    (
      keys_changed.product([:changed]) +
      keys_removed.product([:removed]) +
      keys_added.product([:added])
    ).to_h
  end

  def find_or_create_author(slug)
    Author.by_slug(slug).first_or_create! do |author|
      author.slug = slug
      author.email = "#{slug}@kath.de"
      author.display_name = slug.tr('-', ' ').split(' ').map(&:capitalize).join(' ')
      author.publish
    end
  end

  private

  def a_file
    @a_file ||= Tempfile.new('original')
  end

  def b_file
    @b_file ||= Tempfile.new('modified')
  end
end

seeder = Seeder.new
seeder.create_settings
seeder.insert_or_update_entries(Author)
seeder.insert_or_update_entries(Page)
seeder.insert_or_update_entries(Kommentar)
seeder.insert_or_update_entries(Wochenrueckblick)
seeder.insert_or_update_entries(Tagesreport) do |data|
  data[:entries] = data.delete :body
end
