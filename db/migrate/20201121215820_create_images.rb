class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.string :slug
      t.string :caption
      t.text :description
      t.string :cite

      t.timestamps
      t.index ["slug"], name: "index_images_on_slug"
    end

    add_reference :entries, :cover, foreign_key: {to_table: 'images'}
  end
end
