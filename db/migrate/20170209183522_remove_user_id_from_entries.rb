# frozen_string_literal: true

class RemoveUserIdFromEntries < ActiveRecord::Migration[5.0]
  def change
    remove_column :entries, :user_id
  end
end
