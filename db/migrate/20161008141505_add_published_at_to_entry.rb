# frozen_string_literal: true

class AddPublishedAtToEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :published_at, :datetime, index: true
    add_reference :entries, :user, index: true
  end
end
