# frozen_string_literal: true

class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.string :name, index: true
      t.string :type, index: true
      t.jsonb :payload

      t.timestamps
    end

    add_index :entries, :payload, using: 'gin'
  end
end
