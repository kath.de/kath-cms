# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :username
      t.jsonb :settings

      t.timestamps
    end
    add_index :accounts, :username
    add_index :accounts, :settings, using: 'gin'
  end
end
