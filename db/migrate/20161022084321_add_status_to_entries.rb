# frozen_string_literal: true

class AddStatusToEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :status, :string, default: 'draft', index: true
  end
end
