# frozen_string_literal: true

class CreateAuthorReferences < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :author_ids, :integer, array: true, default: []
    add_index :entries, :author_ids, using: 'gin'
  end
end
