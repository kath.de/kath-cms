# frozen_string_literal: true

class RenameNameToSlugInEntries < ActiveRecord::Migration[5.0]
  def change
    rename_column :entries, :name, :slug
  end
end
