---
date: 2017-02-07
author: kerstin-barton
status: draft
aufmacher_title: "Keine Hilfe vom Vatikan: Ermittler beklagen mangelnde Kooperation"
aufmacher_body: >
    Die Kommission, die sich mit den Missbrauchsvorwürfen gegen die katholischen Kirche in Australien beschäftigt, hat nun konkrete Zahlen zu den Opfern veröffentlich. Demzufolge wurden seit 1950 rund 4.500 Kinder Opfer sexueller Übergriffe geworden. Die Kommission ist bereits 2013 ins Leben gerufen worden und soll in den kommenden Wochen ihre Arbeit abschließen. Leitende Ermittler beklagten die mangelnde Bereitschaft des Vatikans die Ermittlungen durch die Aushändigung geforderter Dokumente und Informationen zu unterstützen. Vertreter der katholischen Kirche in Australien zeigten sich derweilen entsetzt und beschämt über die Veröffentlichung der Zahlen.
---
"Themen des Tages":
- titel: 'EU: „Flüchtlingspolitik zeigt Konstruktionsfehler“'
  url: http://de.radiovaticana.va/news/2016/03/08/eu_%E2%80%9Efl%C3%BCchtlingspolitik_zeigt_konstruktionsfehler%E2%80%9C/1213859
  datum: "2016-03-08"
- titel: Der Islam gehört seit Jahrhunderten zu Russland
  datum: "2016-03-08"
  url: http://www.explizit.net/Religionen/Der-Islam-gehoert-seit-Jahrhunderten-zu-Russland
- titel: Atheismus kann religiöse Gewalt nicht beenden
  datum: "2016-03-06"
  url: http://hinsehen.net/2016/03/06/atheismus-kann-religioese-gewalt-nicht-beenden/
"Pressespiegel":
- titel: Freisinger Bischofskonferenz
  untertitel: Bayerische Bischöfe treffen sich in Passau
  datum: "2016-03-08"
  url: http://www.br.de/nachrichten/niederbayern/inhalt/passau-konferenz-bayerische-bischoefe-100.html
- titel: Online-Petition für zurückgetretenen Pfarrer
  untertitel: Bürger wollen Olivier Ndjimbi-Tshiende halten
  datum: "2016-03-08"
  url: http://www.welt.de/regionales/bayern/article153060119/Buerger-wollen-Pfarrer-mit-Online-Petition-halten.html
- titel: Eine Strategie fürs Religiöse
  untertitel: Entwicklungshilfe und Religion
  datum: "2016-03-08"
  url: http://www.fr-online.de/kultur/entwicklungshilfe-und-religion-eine-strategie-fuers-religioese,1472786,33924804.html
- titel: '„Wir müssen die Katholiken erreichen, die sonst nur über den Gehaltszettel Kontakt zu uns haben“'
  untertitel: Neue Wege im Bistum Essen
  datum: "2016-03-08"
  url: http://www.derwesten.de/staedte/essen/zahl-der-kirchenaustritte-geht-in-essen-zurueck-id11631568.html
- titel: Farbanschlag auf Benedikt-Büste
  untertitel: Ermittlungen bisher ohne Erfolg
  datum: "2016-03-08"
  url: http://www.traunsteiner-tagblatt.de/home_artikel,-Wieder-Farbanschlag-auf-Benedikt-Bueste-_arid,260212.html
"Bistümer und Werke":
- titel: Geflüchtete brauchen besonderen Schutz
  untertitel: KDFB-Erklärung zum Internationalen Frauentag
  datum: "2016-03-08"
  url: http://www.drs.de/service/nachrichten/a-gefluechtete-brauchen-besonderen-schutz-00005851.html
- titel: Flower to the People
  untertitel: Fair gehandelte Rosen zum Weltfrauentag
  datum: "2016-03-08"
  url: https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/flower-to-the-people.html
- titel: '„Alles wieder klar?!“'
  untertitel: "Arbeitshilfe zum Sonntag der Jugend im Erzbistum Bamberg vorgestellt: Thema Barmherzigkeit"
  datum: "2016-03-08"
  url: http://medien.kirche-bamberg.de/pressearchiv-001/%E2%80%9Ealles-wieder-klar%E2%80%9C/09c069fc-7166-41cb-a3e8-a1a0b1eb292a?mode=detail&targetComponent=newslist&startDate=03.2016
- titel: Gemeinsame Sitzung des Senats von Berlin und Leitung des Erzbistums Berlin
  untertitel: Müller und Koch betonen soziale Verantwortung in wachsender Stadt
  datum: "2016-03-08"
  url: http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen/pressemeldung/datum/2016/03/08/gemeinsame-sitzung-des-senats-von-berlin-und-leitung-des-erzbistums-berlinspanmueller-und-koch-bet/
- titel: SEIN. ANTLITZ. KÖRPER.
  untertitel: Kirchen öffnen sich der Kunst
  datum: "2016-03-08"
  url: http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen/pressemeldung/datum/2016/03/08/seinantlitzkoerper-kirchen-oeffnen-sich-der-kunst/
- titel: '„Wo bist Du!“'
  untertitel: "Ökumenischer Kreuzweg der Jugend 2016"
  datum: "2016-03-08"
  url: http://www.bistum-dresden-meissen.de/aktuelles/oekumenischer-jugendkreuzweg-2016.html
- titel: Glauben macht Leben reicher, größer und voller
  untertitel: Am 12. März werden sechs Männer zu Diakonen geweiht
  datum: "2016-03-08"
  url: https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/glauben-macht-leben-reicher-groesser-und-voller.html
- titel: Erzbischof Schick feiert mit vier Priestern Goldenes Weihejubiläum
  untertitel: Luitgar Göller, Herbert Hauf, Friedrich Schmitt und Günther Raab wurden 1966 zu Priestern geweiht
  datum: "2016-03-08"
  url: http://erzbischof.kirche-bamberg.de/meldungen/erzbischof-schick-feiert-mit-vier-priestern-goldenes-weihejubilaeum/5495eae1-6778-49a0-9dfb-cfebf21b1a1d?mode=detail&targetComponent=newslist
- titel: Hat der Himmel ein Ende? - Neues Familienbuch des Bonifatiuswerkes
  untertitel: Die Vielfalt der Schöpfung entdecken und bewahren
  datum: "2016-03-08"
  url: http://www.bonifatiuswerk.de/werk/aktuelles/newsausgabe/article/wir-entdecken-die-schoepfung-neues-familienbuch-des-bonifatiuswerkes/
- titel: R.SA bietet Kirchen-Special zum Karfreitag
  untertitel: "Zweitausend Jahre in zehn Stunden: „Von der Kreuzigung Jesu bis Papst Franziskus“"
  datum: "2016-03-08"
  url: http://www.bistum-dresden-meissen.de/aktuelles/radio-r.sa-bietet-kirchen-spezial-zum-karfreitag.html
- titel: Bistum Hildesheim mit neuem Internetauftritt
  untertitel: Moderne Gestaltung für alle Bildschirmgrößen setzt neue Schwerpunkte
  datum: "2016-03-08"
  url: https://www.bistum-hildesheim.de/bistum/nachrichten/artikel/news-title/bistum-hildesheim-mit-neuem-internetauftritt-6470/
