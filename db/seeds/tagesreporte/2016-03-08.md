---
author: lukas-ansorge
date: 2016-03-08
aufmacher_title: Bischofskonferenz in Passau
aufmacher_body: >
  Vom 8. bis 10. März treffen sich die sieben bayerischen Bischöfe zu ihrem Frühjahrstreffen in Passau. Ihre Gesprächsthemen sind die Ökumene, die Situation der katholischen Universität Eichstätt-Ingolstadt, die Jugendarbeit sowie die Frage, wie die Kirche orientalische Christen in Bayern besser unterstützen kann. Zum Abschluss der Konferenz wird es zwei Gottesdienste geben, zu denen die Gläubigen eingeladen sind.
---
Themen des Tages:
- titel: 'EU: „Flüchtlingspolitik zeigt Konstruktionsfehler“'
  url: http://de.radiovaticana.va/news/2016/03/08/eu_%E2%80%9Efl%C3%BCchtlingspolitik_zeigt_konstruktionsfehler%E2%80%9C/1213859
- titel: Der Islam gehört seit Jahrhunderten zu Russland
  url: http://www.explizit.net/Religionen/Der-Islam-gehoert-seit-Jahrhunderten-zu-Russland
- titel: Atheismus kann religiöse Gewalt nicht beenden
  datum: 2016-03-06
  url: http://hinsehen.net/2016/03/06/atheismus-kann-religioese-gewalt-nicht-beenden/
Pressespiegel:
- titel: Freisinger Bischofskonferenz
  untertitel: Bayerische Bischöfe treffen sich in Passau
  url: http://www.br.de/nachrichten/niederbayern/inhalt/passau-konferenz-bayerische-bischoefe-100.html
- titel: Online-Petition für zurückgetretenen Pfarrer
  untertitel: Bürger wollen Olivier Ndjimbi-Tshiende halten
  url: http://www.welt.de/regionales/bayern/article153060119/Buerger-wollen-Pfarrer-mit-Online-Petition-halten.html
- titel: Eine Strategie fürs Religiöse
  untertitel: Entwicklungshilfe und Religion
  url: http://www.fr-online.de/kultur/entwicklungshilfe-und-religion-eine-strategie-fuers-religioese,1472786,33924804.html
- titel: '„Wir müssen die Katholiken erreichen, die sonst nur über den Gehaltszettel Kontakt zu uns haben“'
  untertitel: Neue Wege im Bistum Essen
  url: http://www.derwesten.de/staedte/essen/zahl-der-kirchenaustritte-geht-in-essen-zurueck-id11631568.html
- titel: Farbanschlag auf Benedikt-Büste
  untertitel: Ermittlungen bisher ohne Erfolg
  url: http://www.traunsteiner-tagblatt.de/home_artikel,-Wieder-Farbanschlag-auf-Benedikt-Bueste-_arid,260212.html
Bistümer und Werke:
- titel: Geflüchtete brauchen besonderen Schutz
  untertitel: KDFB-Erklärung zum Internationalen Frauentag
  url: http://www.drs.de/service/nachrichten/a-gefluechtete-brauchen-besonderen-schutz-00005851.html
- titel: Flower to the People
  untertitel: Fair gehandelte Rosen zum Weltfrauentag
  url: https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/flower-to-the-people.html
- titel: '„Alles wieder klar?!“'
  untertitel: "Arbeitshilfe zum Sonntag der Jugend im Erzbistum Bamberg vorgestellt: Thema Barmherzigkeit"
  url: http://medien.kirche-bamberg.de/pressearchiv-001/%E2%80%9Ealles-wieder-klar%E2%80%9C/09c069fc-7166-41cb-a3e8-a1a0b1eb292a?mode=detail&targetComponent=newslist&startDate=03.2016
- titel: Gemeinsame Sitzung des Senats von Berlin und Leitung des Erzbistums Berlin
  untertitel: Müller und Koch betonen soziale Verantwortung in wachsender Stadt
  url: http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen/pressemeldung/datum/2016/03/08/gemeinsame-sitzung-des-senats-von-berlin-und-leitung-des-erzbistums-berlinspanmueller-und-koch-bet/
- titel: SEIN. ANTLITZ. KÖRPER.
  untertitel: Kirchen öffnen sich der Kunst
  url: http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen/pressemeldung/datum/2016/03/08/seinantlitzkoerper-kirchen-oeffnen-sich-der-kunst/
- titel: '„Wo bist Du!“'
  untertitel: "Ökumenischer Kreuzweg der Jugend 2016"
  url: http://www.bistum-dresden-meissen.de/aktuelles/oekumenischer-jugendkreuzweg-2016.html
- titel: Glauben macht Leben reicher, größer und voller
  untertitel: Am 12. März werden sechs Männer zu Diakonen geweiht
  url: https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/glauben-macht-leben-reicher-groesser-und-voller.html
- titel: Erzbischof Schick feiert mit vier Priestern Goldenes Weihejubiläum
  untertitel: Luitgar Göller, Herbert Hauf, Friedrich Schmitt und Günther Raab wurden
    1966 zu Priestern geweiht
  url: http://erzbischof.kirche-bamberg.de/meldungen/erzbischof-schick-feiert-mit-vier-priestern-goldenes-weihejubilaeum/5495eae1-6778-49a0-9dfb-cfebf21b1a1d?mode=detail&targetComponent=newslist
- titel: Hat der Himmel ein Ende? - Neues Familienbuch des Bonifatiuswerkes
  untertitel: Die Vielfalt der Schöpfung entdecken und bewahren
  url: http://www.bonifatiuswerk.de/werk/aktuelles/newsausgabe/article/wir-entdecken-die-schoepfung-neues-familienbuch-des-bonifatiuswerkes/
- titel: R.SA bietet Kirchen-Special zum Karfreitag
  untertitel: "Zweitausend Jahre in zehn Stunden: „Von der Kreuzigung Jesu bis Papst Franziskus“"
  url: http://www.bistum-dresden-meissen.de/aktuelles/radio-r.sa-bietet-kirchen-spezial-zum-karfreitag.html
- titel: Bistum Hildesheim mit neuem Internetauftritt
  untertitel: Moderne Gestaltung für alle Bildschirmgrößen setzt neue Schwerpunkte
  url: https://www.bistum-hildesheim.de/bistum/nachrichten/artikel/news-title/bistum-hildesheim-mit-neuem-internetauftritt-6470/
