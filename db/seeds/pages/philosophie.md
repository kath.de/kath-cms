---
long_title: Die kath.de-Philosophie
title: Philosophie
---

kath.de steht für Werte

* Information über die Themen und Ereignisse mit Bezug zu Religion und Kirche;
* Spiritualität und Liturgie mit einem Schwerpunkt am Wochenende;
* Eine täglicher Überblick, wie kirchliches Sprechen und Handeln in der Öffentlichkeit wahrgenommen wird;
* Bistümern, Akademien, Bildungseinrichtungen, Orden und Medien einen besseren Zugang zur Internet-Öffentlichkeit ermöglichen
* Über die Lexika christliche Lebensform, theologisches Wissen, Symbolik, Kirchenjahr zugänglich machen.

kath.de arbeitet auf der Basis der kirchlichen Dokumente über die Medien. Das II. Vatikanische Konzil hat die Abfassung von Communio et Progressio veranlasst. Darin wird der Aufbau der menschlichen Gemeinschaft als Ziel der durch Medien ermöglichten Kommunikation definiert. Da eine Urteilsbildung als Basis die Präsentation der relevanten Standpunkte verlangt, bietet kath.de ein breites Spektrum an Positionen. Das ist kath.de auf Grund seiner Unabhängigkeit möglich und hat zu dem Vertrauen in die unvoreingenommene Berichterstattung beigetragen.

kath.de fühlt sich nicht nur den von der Kirche formulierten Kriterien für eine verantwortungsbewusste Berichterstattung verpflichtet, sondern stellt sich den neuen Herausforderungen. Die Krisen der Moderne haben jedem vor Augen geführt, dass die Gesellschaft auf die Verwirklichung von Werten angewiesen ist und dass nicht zuletzt wirtschaftliches Handeln ohne eine Wertedimension destruktiv für das menschliche Zusammenleben wird.

kath.de verpflichtet sich daher, mit Autoren zusammenzuarbeiten, die sich diesen Fragen widmen. Ziel ist es, dass im Diskurs zu klärende ethische Standpunkte und argumentativ vertretene Wertorientierungen zugänglich gemacht werden. Die Kommentierung von aktuellen Vorgängen erfolgt auf dem Hintergrund des biblischen Menschenbildes. Da Werte nicht nur durch Argumentation vermittelt, sondern in Riten und Gottesdiensten erlebt werden müssen, um auch emotionale Überzeugungskraft zu gewinnen, widmet sich kath.de der Förderung der Liturgie.

Der Mehrwert für die Nutzer von kath.de wird dadurch erreicht, dass Werte, nicht zuletzt im Zusammenhang mit wirtschaftlichen Fragen, stärker herausgearbeitet und die Bedeutung von Werten für verschiedene Lebensbereiche argumentativ ins Gespräch gebracht werden. kath.de sucht die Kooperation mit Partnern, die sich an dieser Werte-Initiative beteiligen. Hohe journalistische Professionalität und die beruflichen ethischen Standards prägen die Arbeitsweise. Die Einarbeitung von journalistischen Nachwuchskräften mit christlich-kirchlichem Interesse genießt einen hohen Stellenwert.

*Mai 2009, Theo Hipp, Eckhard Bieger SJ*
