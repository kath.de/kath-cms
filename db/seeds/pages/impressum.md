---
title: Impressum
subtitle: kath.de &ndash; online seit dem 29. Februar 1996
---

<p class="einleitung">Der Internetdienst <strong>kath.de</strong> ist nicht die katholische Kirche im Internet, sondern beruht auf der privaten Initiative zahlreicher katholischer Christen. Dank ihrer Mithilfe kann <strong>kath.de</strong> täglich neu einen unabhängigen, aktuellen Nachrichtenüberblick zu Themen zusammenstellen, die die katholische Kirche in Deutschland und der Welt betreffen. Kirchliche Einrichtungen, Bistümer, Verlage, Medienstellen und andere im Sinne der christlichen Glaubensprinzipien Tätigen ergänzen diese Informationen mit eigenen Meldungen.</p>

<dl>
  <dt>Redaktionsleitung und Ansprechpartner</dt>
  <dd>Maximilian Röll <a href="mailto:roell@kath.de">roell@kath.de</a></dd>
  <dt>V.i.S.d.P.</dt>
  <dd>Theo Hipp <a href="mailto:hipp@kath.de">hipp@kath.de</a></dd>
  <dt>Webentwicklung</dt>
  <dd>Johannes Müller <a href="mailto:johannes.mueller@kath.de">johannes.mueller@kath.de</a></dd>
  <dt>Systemadministration</dt>
  <dd>Johannes Müller</dd>
</dl>

<p><strong><a href="/redaktion">Das kath.de-Team</a></strong></p>


<h2>Pflichtangaben nach § 6 Teledienstgesetz:</h2>

Betreiber dieses Dienstes:
<address>
kath.de gGmbH<br>
Marienweg 9<br>
78567 Fridingen<br>
Geschäftsführer: Theo Hipp<br>
Email: hipp@kath.de<br>
Tel: 0171 / 5247893
</address>

<p>Die kath.de gGmbH hat beim Amtsgericht Stuttgart die Handelsregisternummer HRB 735487.
Steuernummer: 21107/5151.</p>

<h2>Urheberrechtshinweis</h2>
<p>Das Layout und die Inhalte des Internetauftritts von www.kath.de inklusive aller Unterseiten und Subdomains, besonders die Zusammenstellung der Meldungen und eigene Texte der Redaktion, sind urheberrechtlich geschützt. Alle Rechte sind vorbehalten. Mit Ausnahme der angebotenen Downloads zur Verwendung in der Originalfassung verstößt die Verwendung der Texte und Abbildungen, auch auszugsweise, ohne die vorherige schriftliche Zustimmung der kath.de GmbH &amp; Co. KG gegen die Bestimmungen des Urheberrechts und ist damit rechtswidrig. Dies gilt insbesondere auch für alle Verwertungsrechte wie die Vervielfältigung, die übersetzung oder die Verwendung in elektronischen Systemen. Für auf unserer Website verwendete eingetragene Marken, Handelsnamen, Gebrauchsmuster und Logos gelten die entsprechenden gesetzlichen Bestimmungen, auch wenn sie an den jeweiligen Stellen nicht als solche gekennzeichnet sind.</p>

<h2>Haftungshinweis</h2>
<h3>Inhalte</h3>
<p>Die Inhalte dieser Seiten wurden sorgfältig bearbeitet und überprüft. Der Betreiber übernimmt jedoch keine Gewähr für die Aktualität, Richtigkeit, Vollständigkeit noch für die Qualität der bereitgestellten Informationen. Haftungsansprüche gegen den Betreiber, die sich auf Schäden materieller oder ideeller Art beziehen, welche durch Nutzung oder Nichtnutzung der dargebotenen Informationen oder durch fehlerhafte und unvollständige Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Betreibers kein nachweislich vorsätzliches oder grob fahrlässige Verschulden vorliegt. Der Betreiber behält sich ausdrücklich das Recht vor, Teile der Seite oder das gesamte Angebot ohne vorherige Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung einzustellen. Der Betreiber ist nicht verantwortlich für den Inhalt, die Verfügbarkeit, die Richtigkeit und die Genauigkeit der verlinkten Seiten, deren Angebote, Links und Werbeanzeigen. Der Betreiber haftet nicht für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere nicht für Schäden, die durch Nutzung oder Nichtnutzung der auf den verlinkten Seiten angebotenen Informationen entstehen.</p>

<h3>Urheberrecht</h3>
<p>Der Betreiber ist bestrebt, in allen Publikationen geltende Urheberrechte zu beachten. Sollte es trotzdem zu einer Urheberrechtsverletzung kommen, wird der Betreiber das entsprechende Objekt nach Benachrichtigung aus seiner Publikation entfernen bzw. mit dem entsprechenden Urheberrecht kenntlich machen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützter Marken- bzw. Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichnungsrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind. Das Urhebrecht für die Inhalte dieses Forums steht allein den Autoren zu. Eine Vervielfältigung solcher Texte, Grafiken oder Sounds in elektrischen und gedruckten Publikationen ist ohne ausdrückliche Zustimmung des jeweiligen Beitragsschreibers nicht gestattet. Der Betreiber behält sich allerdings vor, aus den Einträgen zu zitieren.</p>

<h3>Rechtswirksamkeit</h3>
<p>Dieser Haftungsauschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wird. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokuments in ihrem Inhalt und ihrer Gültigkeit hiervon unberührt.</p>

<h3>Datenschutz</h3>
<p>kath.de nimmt den Schutz Ihrer persönlichen Daten ernst und hält sich strikt an die Regeln der Datenschutzgesetze. Personenbezogene Daten werden auf dieser Webseite nur im technisch notwendigen Umfang erhoben. In keinem Fall werden die erhobenen Daten verkauft oder aus anderen Gründen an Dritte weitergegeben. Konkret heißt das: kath.de speichert personenbezogene Daten nur als Abonnenten der Newsletter sowie die mit ihrem Profil eingetragenen Nutzer in einer der Plattformen.</p>
<p>Jeder Nutzer einer Plattform bzw. jeder, der sich als Empfänger eines Newsletters eingetragen hat, kann auf einfache Weise sein Profil auf der jeweiligen Plattform löschen und sich aus dem Adressatenkreis des Newsletters austragen.</p>
<p>Die Daten werden von kath.de nicht an Dritte weitergegeben und auch nicht anderen für Werbezwecke zur Verfügung gestellt. Bei der Nutzung der Eingangsseite erhebt und speichert kath.de automatisch Log Files-Informationen, die Ihr Browser an uns übermittelt.</p>
Dies sind:
<ul>
  <li>Browsertyp/-version</li>
  <li>verwendetes Betriebssystem</li>
  <li>Referrer URL (die zuvor besuchte Seite)</li>
  <li>Hostname des zugreifenden Rechners (IP Adresse)</li>
  <li>Uhrzeit der Serveranfrage.</li>
</ul>
<p>Diese Daten können durch kath.de nicht bestimmten Personen zugeordnet werden (s.u. Technischer Hinweis zur Identifizierung des Nutzers einer IP-Adresse). Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen, die Daten werden zudem nach einer statistischen Auswertung gelöscht.</p>

<h4>Cookies</h4>
<p>Die Internetseiten verwenden an mehreren Stellen so genannte Cookies. Sie dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert. Die meisten der von uns verwendeten Cookies sind so genannte "Session-Cookies". Sie werden nach Ende Ihres Besuchs automatisch gelöscht. Cookies richten auf Ihrem Rechner keinen Schaden an und enthalten keine Viren. Cookies werden auch im Zusammenhang mit Nutzungsdaten eingesetzt.</p>
<h4>Messung der Nutzung einzelner Seiten</h4>
<p>kath.de braucht einige Daten vom Nutzerverhalten der Seite, um aus diesen Daten das Angebot zu optimieren. Dafür nutzt kath.de den Webanalysedienst Google Analytics. Dieser verwendet ebenfalls  "Cookies", Textdateien, die auf dem Computer des Nutzers gespeichert werden und die eine Analyse der Benutzung von kath.de ermöglichen. Hierzu erklärt Google:</p>
<blockquote>Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich Ihrer IP-Adresse) werden an einen Server von Google in den USA übertragen und dort gespeichert. Google wird diese Informationen benutzen, um die Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten für die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte übertragen, sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten von Google in Verbindung bringen. Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern. Durch die Nutzung dieser Website erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden.</blockquote>

<h4>Technischer Hinweis zur Identifizierung des Nutzers einer IP-Adresse</h4>
<p>Die erhobenen Daten über einen Computer, der eine Homepage ansteuert, beinhaltet nur den Ort, und die Zeit. Der Besitzer eines Computers kann über die Ip-Adresse nur identifiziert werden, wenn der Provider (also Arcor, T-online, Alice o.a.) die Daten herausgibt. Diese Sicherung wird dadurch erreicht, dass der Provider mit jeder Session, d.h. wenn der PC jeweils neu mit dem Internet verbunden ist, diesem jeweils eine andere Identifizierung zuweist. Sie sind also mit wechselnden Absenderinformationen im Netzt. Damit wird verhindert, dass Anbieter wie kath.de ein personalisiertes, einem bestimmten Nutzer zuschreibbares Profil der Besucher ihrer Seiten erstellen können. Nur Ihr Provider kann die Ihnen jeweils zugeteilte IP-Adresse mit ihren persönlichen Daten in Verbindung bringen.</p>
<h4>Newsletter</h4>
<p>Wenn Sie einen über kath.de angebotenen Newsletter empfangen möchten, benötigen wir von Ihnen eine Email-Adresse sowie Informationen, die uns die Überprüfung gestatten, dass Sie der Inhaber der angegebenen Email-Adresse sind bzw. deren Inhaber mit dem Empfang des Newsletters einverstanden ist. So kann niemand, der Ihre Emailadresse kennt, für Sie einen Newsletter abonnieren. Dies geschieht dadurch, dass Sie, bevor der erste Newsletter an Sie versandt wird, Sie eine Mail an Ihre Emailadresse erhalten, mit der Sie Ihre eingegebene Adresse bei kath.de erst freischalten. Weitere Daten werden nicht erhoben.
Ihre Einwilligung zur Speicherung der Daten, der Email-Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen, indem Sie über die Eingabemaske den Newsletter einfach abbestellen.</p>

<p>Weitere Auskünfte zum Datenschutz können sie gerne bei der Redaktion (zum <a href="kontakt.php">Kontaktformular</a>) einholen.
