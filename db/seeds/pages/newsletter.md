---
title: Newsletter
long_title: Der kath.de-Newsletter
---

<p class="aufmerksamkeit">Unser wöchentlicher Themenrückblick informiert Sie über die aktuellen Entwicklungen in Kirche und Gesellschaft, macht auf Tagungen und Seminare sowie Publikationen und Titel aufmerksam. Sie könne sich hier zum Newsletter anmelden und erhalten dann immer Donnerstags<!-- TODO --> eine E-Mail.</p>

<form>
  <label for="email">E-Mail-Adresse</label>
  <input type="email" name="email" />

  <button type="submit">Zum Newsletter anmelden</button>
</form>
