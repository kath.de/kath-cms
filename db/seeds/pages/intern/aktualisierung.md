---
title: Aktualisierung kath.de
status: hidden
author: maximilian-roell
---

# A. Redaktionspostfach überprüfen

Redaktionspostfach bis 19 Uhr auf aktuelle Meldungen überprüfen.

<a href="https://www.kath.de/webmail" class="btn">Redaktionspostfach</a>

# 1. Vor der Redaktion
**Kurzanleitung für die Redaktion**: Alle wichtigen Schritte der Redaktion in Kurzform: [Kurzanleitung.pdf](kurzanleitung.pdf)

**index.php**: Aktuelle index.html vom Server holen. Um Vorschau im HTML-Editor zu ermöglichen auch katholisch.css und katholisch.js runterladen.

**Redaktionspostfach bearbeiten**: Das Redaktionspostfach generell täglich bearbeiten:

* sind Redakteure direkt angesprochen, die Mail an diese weiterleiten.
* inhaltliche Mails von Radio Vatikan an <deutsch@vatiradio.va>
* technische Mails bezügl. RV-Newsletter (An-/Abmeldung, techn. Anfragen): <newsletter-deutsch@vatiradio.va>
* Spam löschen
* Anmeldungen für (nicht RV-) Newsletter und technischen Anfragen an <radej@kath.de>
* Anmerkungen zur Redaktion und Anfragen zu kath.de an roell@kath.de

# 2. Themen des Tages
**Radio Vatikan**: Aktualisierung der Tagesmeldung Radio Vatikan.

<http://www.oecumene.radiovaticana.org/ted/index.asp>

NEU: EINE MELDUNG VON DIESER SEITE AUSWÄHLEN!, DIESE verlinken, Datum anpassen

**explizit.net:** Tagesaktuelle Meldung von <http://explizit.net> aussuchen und verlinken.

Link mit Titel und Untertitel einfügen


## Fr (Sa u. So stehenlassen)

**Den aktuellen Wochenkommentar**: aus dem BLOG! verlinken (Artikel-Link!). Den Redakteur des Kommentars aktualisieren und mit Teamseite verlinken. ( <www.kath.de/seiten/team.html#xx> --- xx=Initialien des Redakteurs (in Minuskeln)

## Fr (Sa stehenlassen)

Wochenrückblick den aktuellen Wochenrückblick verlinken (der Link bleibt immer gleich.) Den Redakteur aktualisieren und mit Teamseite verlinken. ( )

## Fr-So

* hinsehen.net <http://hinsehen.net/>

# 4. Service & Spiritualität

## Mi:
* Update Seele <http://www.update-seele.de/de/impuls-der-woche/> (der Link bleibt immer gleich)

## Do:

* Meditation Freiburg Rubrik "Spiritualität" / "Meditation zum Sonntagsevangelium".

Falls der Titel anders ist: "Meditation zum Evangelium des kommenden Sonntags" als Untertitel aktivieren

## Fr (Sa stehenlassen):
* **Fürbitte zum Sonntag:** Aktuelle Fürbitte auswählen und Verlinken. Text lautet: Fürbitten zum XX. Sonntag im Jahreskreis / zum Fest (ANPASSEN!)

Die aktuelle Fürbitten findet ihr hier: <http://www.bistum-trier.de/glaube-spiritualitaet/gottesdienst/fuerbitten/>

## Fr (Sa stehenlassen):

* ZDF-Fernsehgodi Detail <http://www.zdf.fernsehgottesdienst.de/>

–> auf die Seite des jeweiligen Gottesdienstes direkt verlinken, also z.B. <http://www.fernsehgottesdienst.de/52_4255.htm>

## Fr (Sa stehenlassen):

**Gedanken und Predigten zum Sonntag / zum Fest (Text in der index-Datei entsprechend ändern!)**

* **Titelzeile:** <http://erzabtei-beuron.de/liturgie/index.php>

*–> in der Monatstabelle auf den jeweiligen Sonn-/ Feiertag klicken und Link von da übernehmen. Schriftstellen der 1. und 2. Lesung und Evangelium von <http://erzabtei-beuron.de/liturgie/index.php> übernehmen*

* M. Bolowich <http://www.innenstadtkirche.kirche-bamberg.de/impulse/predigten> *Link bleibt immer gleich*
* perikopen.de <http://www.perikopen.de/> *Autorenname ändern!*
* Catena aurea <http://www.catena-aurea.de/> *Wenn der aktuelle Text nicht auf der Startseite verlinkt ist: Bitte im jeweiligen Lesejahr nachsehen.*
* J. Kuhlmann <http://www.kath.de/predigt/jk/lesung_c.htm> *–> richtigen Sonntag / Fest auswählen*
* M. Löwenstein <http://www.martin-loewenstein.de/> *Aktuelle Predikt*
* Frauenpredigten <http://www.kath-frauenpredigten.de> *Bitte immer die "aktuelle Predigt" verlinken und deren Titel eintragen.*<br/> **Grundsätzlich bitte NUR AKTUELLE PREDIGTEN VERLINKEN!**
* ggf. samstags noch nicht verlinkte Predigt-Seiten überprüfen und aktuelle Predigten verlinken

## täglich:
* **Kirchenjahr-Lexikon_** zu Festen etc.: am Vorabend jeweils den entsprechenden Artikel aus dem Lexikon "Kirchenjahr und Brauchtum" bringen
* **Spiritueller Beitrag:** unregelmäßig; per Mail an die Redaktionsadresse oder Hinweis im Intranet

# 5. Bistümer & Werke
Grundsätzlich gilt: So wie Platz ist, werden ALLE TAGESAKTUELLEN verlinkt - wenn nicht genügend Platz ist, die am wenigsten relevanten löschen!

## täglich
* **Erzbistum Berlin:** <http://www.erzbistumberlin.de/medien/pressestelle/aktuelle-pressemeldungen>
* **Erzbistum Freiburg:** <http://www.erzbistum-freiburg.de/html/aktuell/aktuell436.html?t=edfeb5ed817a9eea49c3a919c7c86011&>
* **Bistum Fulda:** <http://www.bistum-fulda.de/bistum_fulda/presse_medien/aktuelle_meldungen.php>
* **Bistum Limburg:** <http://www.bistumlimburg.de/index.php?_1=0&_0=15>
* **Erzbistum Bamberg:** <http://www.erzbistum-bamberg.de/>
* **Bistum Mainz:** <http://www.bistummainz.de/bm/dcms/sites/bistum/bistum/ordinariat/dezernate/dezernat_Z/pressestelle/index.html>
* **Bistum Hildesheim:** <https://www.bistum-hildesheim.de/bistum/nachrichten/>
B. Rottenburg-Stuttgart <http://www.drs.de/service/nachrichten.html>
* **Bistum Dresden-Meißen:** <http://www.bistum-dresden-meissen.de/front_content.php?idcat=1579>
* **Bistum Erfurt:** <http://www.bistum-erfurt.de/front_content.php?idcat=1836>
* **Bistum Görlitz:** <http://bistum-goerlitz.de/?page_id=1250>
* **Bistum Magdeburg:** <http://www.bistum-magdeburg.de/>
* **Renovabis:** <https://www.renovabis.de/service/presse/aktuelles>
* **Bonifatiuswerk:** <http://www.bonifatiuswerk.de>

keine Meldungen von externen Quellen, die wir auch haben / schon hatten, z.B. Radio Vatikan, Bistümer etc.

## Sa, So:

* **Tag des Herrn:** <http://www.tag-des-herrn.de/> –> zwei überregional interessante Beiträge am Wochenende. Datumsangabe: "Tag des Herrn, Datum". Immer nur eine Meldung pro Tag!

**am Ende: **: Artikel sortieren

# 6. Pressespiegel mit kirchlichen Medien!

**Google News** <http://news.google.de/>
Suchwörter: Papst, Vatikan, Kardinal, Bischof, Bistum, Religion, Kirche, Priester, Ökumene - bsp. Evangelische Kirche, Orthodoxe Kirche

Bei Auswahl darauf achten, dass das Interesse nicht zu lokal ist, und eine ausgewogene Auswahl entsteht.
Also nicht nur Papst, aber auch nicht nur Bistumsmeldungen.
Bei Bedarf aktuelle Begriffe zusätzlich, z.B. Weltjugendtag, Katholikentag, Kirchentag, ... – Hinweise immer gerne an Maximilian!)

**Quellenliste.**
Bitte darauf achten, dass keine Medien genommen werden, die auf der "Nicht"-Liste stehe.

außerdem:

* <http://kirchensite.de/aktuelles/kirche-heute/> *–> KNA-Meldungen unter "Kirche heute"; Quellenangabe: "kirchensite | KNA"*
* <http://www.domradio.de>

## Do:
* **Christ in der Gegenwart** <http://www.christ-in-der-gegenwart.de/aktuell/artikel_html?wkz=CGWWW11&campaign=KATHDE> *(Der Link bleibt immer gleich; **Quelle/Datum:** "Christ in der Gegenwart Nr. mm/jjjj" – Die Nummer der Ausgbe steht **unter** dem Text des Artikels: "CiG mm/jjjj") **Wichtig:** Titel ist nie "Aktueller Artikel"*

Meldungen aus der Herder-Korrespondenz oder aus Stimmen der Zeit unter Pressespiegel bringen. Link kommt per Mail.

**am Ende:** Artikel NACH RELEVANZ (!) sortieren

# 7. Videotipp des Tages

TV-Tipp-Banner
Direkt unter dem Ende des Pressespiegels

<!--Beginn Tv-Tipp -->
 <a href="http://www.kath.de/tv-kirche" rel="nofollow"><img src="kirchetv/tv-tipp-2012-07-14.jpg" >

Bitte täglich das Datum auf den folgenden Tag aktualisieren.
Falls im Ordner "kirchetv" kein aktuelles Bild ist, bitte "Fernsehtipp_kathde_neutral.jpg" einstellen

# 8. Aufmacher

neuen Aufmacher schreiben ([Anleitung PDF](aufmacherschreiben.pdf)), Datum aktualisieren!
Am Ende besonders aufmerksam gegenlesen (Tipp- und Schreibfehler etc., Datum geändert?)

Bitte nicht dasselbe Thema wie Radio Vatikan-Topmeldung oder explizit.net-Meldung!

# 9. Social Media Redaktion

Am Ende der Tagesredaktion wird der Top-Artikel des Tages zusammen mit einem kurzen Teaser und der Angabe der Bildquelle in folgendem Format auf Facebook eingestellt. Dazu den Teaser zusammen mit der Foto-Quellenangabe in Facebook als Post eingeben und darunter den Link zum Artikel eingeben. Automatisch erscheint eine Artikelvorschau mit dem Foto. Danach den Link wieder löschen und auf posten klicken.

**Teaser:** Ein kurzer, anregender Satz (nicht der selbe wie auf www.kath.de)
**Quellenangabe:** (Foto: XY)
**Artikel:** Bild mit Artikelvorschau - ausgeschriebenen Link bitte löschen

# 10. Aktualisierungsdatum und Redakteur

"Zuletzt aktualisiert am" auf das aktuelle Datum setzen,
passende Redakteurs-Zeile aktivieren

# Nach der Redaktion
* Aufmacher und Pressespiegel vom Tag ins Archiv kopieren
Für den Pressespiegel folgende Vorlage benutzen: <http://kath.de/scripte/presse.php?codesichtbar> --> Per Copy and Paste in die HTML-Ansicht des Typo3-Editors kopieren.
* neue index.php per FTP aufspielen und überprüfen
(Links ausprobieren, stimmen Quelle und Datum jeweils?, Aktualisierungsdatum, Redakteur richtig?, Tippfehler)
* Backup der neuen index.html in den Ordner #BACKUP#/Jahr/Monat/Tag/ auf dem Server kopieren
* Redaktionsmails bearbeiten, falls noch nicht geschehen
* Hier ein Text zu Html falls es Probleme beim Archivieren gibt: Html-Text
