---
author: philipp-mueller
title: Recht gegen Angst
published_at: 2016-01-08
tags: ['Demokratie', 'Rechtsstaat', 'Innere Sicherheit']
teaser_text: >
    Nach der Kölner Silvesternacht müssen wir uns beruhigen und unseres besten Trumpfs besinnen. Der Rechtsstaat unterscheidet uns fundamental von Saaten wie Irak, Syrien oder Eritrea. Er garantiert keine absolute Sicherheit, aber keine Staatsform kann das.
---
<p class="einleitung">Nach der Kölner Silvesternacht müssen wir uns beruhigen und unseres besten Trumpfs besinnen. Der Rechtsstaat unterscheidet uns fundamental von Saaten wie Irak, Syrien oder Eritrea. Er garantiert keine absolute Sicherheit, aber keine Staatsform kann das.</p>

### Gleicher Rechtstaat für alle
Vieles ging nach den Übergriffen am Kölner Hauptbahnhof durcheinander. Vor allem durch die Frage nach der Identität der Täter.

Zeugen berichten, sie seien nicht europäisch stämmig gewesen. Dabei fühlen sie sich genötigt, sich zu erklären. Ein [Zeuge](http://www1.wdr.de/themen/aktuell/koeln-uebergriffe-hauptbahnhof-krisentreffen-100.html) drückt es so aus: „Kommt vielleicht blöd, aber es entspricht einfach der Wahrheit.“ Wie kann es so schwierig und erklärungsbedürftig sein, das Aussehen eines Verdächtigen zu beschreiben?

Das Problem liegt in der Idee einer „sozialen“ Schuld. Dabei wird die Schuld nicht nur bei den Tätern, sondern beim gesamten Umfeld gesehen. Doch wenn Täter einen Migrationshintergrund haben oder Flüchtlinge sind, bedeutet das nicht, dass alle Menschen mit Migrationshintergrund oder alle Flüchtlinge Täter sind. Nur die Täter haben die Straftat begangen.

Es ist dabei irrelevant, welche Hintergründe ein Täter hat. Genauso wie es irrelevant ist welchen Hintergrund ein Opfer hat. Wir haben einen Rechtstaat in Deutschland, der für alle gleich ist.

### Grenzen des Rechtstaats

Weitere Unruhe ging von der Pressekonferenz zu den Vorfällen aus. Kölns Oberbürgermeisterin Henriette Reker beantwortete die Frage einer Journalistin, wie der Schutz vor solchen Übergriffen möglich sei. Die Antwort Rekers, mehr als eine Armlänge Abstand zu halten, führte zu scharfer [Kritik](http://www.tagesschau.de/inland/reker-143.html)in den Social Media. Ihr wird vorgeworfen, den Opfern eine Mitschuld zuzuschieben. Dabei wird die Intention der Antwort übersehen: Ein Opfer einer Gewalttat sucht in einer Lage, die sie überfordert, nach einer Möglichkeit etwas zu tun. Eine Möglichkeit im ausgeliefert-Sein nicht auch noch handlungsunfähig zu bleiben. Wenigstens etwas zu machen. Ob es objektiv hilft ist ungewiss, aber subjektiv wirkt es direkt.

Ein weiter Kritikpunkt ist, das sei keine rechtsstaatliche Lösung. Doch der Rechtsstaat verhindert keine Verbrechen. Er ahndet sie im Nachhinein. Die Verhinderung von Straftaten ist Aufgabe der Sicherheitskräfte, Polizei und Nachrichtendienste. Sie helfen sowohl dem Rechtstaat bei der Aufklärung von Verbrechen, haben darüber hinaus die Aufgabe Prävention zu leisten. Aber auch sie können nicht alle Straftaten verhindern. Ein Restrisiko bleibt immer.

Mit solchen latenten Risiken gehen wir ständig um. Beim Autofahren, beim Sport, aber auch wenn wir in größeren Menschenansammlungen sind. All diese Aktivitäten enthalten Risiken, durch eigenes Verhalten, fremdes Einwirken, aber auch unkontrollierbare Umstände. Das Eingehen dieser Risiken bedeutet keine Zustimmung oder Mitschuld an möglichen Übergriffen. Dennoch sollten uns diese Risiken bewusst sein. Wir sollten wissen, wie wir uns in gefährlichen Situationen selbst helfen können. Wie man sich vor Angriffen schützen kann. Ein unvorbereitetes Opfer trifft keine Schuld. Vorbereitet sein, wollen wir trotzdem. Zu allererst sind wir für uns und unsere Sicherheit verantwortlich. Das ist schon alleine unserem Eigeninteresse geschuldet. Kein Staat der Welt kann uns diese Verantwortung nehmen. Ein demokratischer Rechtsstaat will das auch gar nicht.

### Lichtblick aus der Nacht

Mittlerweile gingen über [200 Anzeigen](http://www.spiegel.de/panorama/justiz/koelner-polizei-nimmt-nach-uebergriffen-zwei-verdaechtige-fest-a-1071100.html) bei der Polizei ein, davon fast dreiviertel wegen sexueller Übergriffe. Eine erschreckend hohe Zahl. Doch es zeugt von einem Vertrauen in den Rechtsstaat. Gerade Sexualdelikte haben eine notorisch hohe Dunkelziffer. Wir müssen den Opfern dankbar sein, sich diesen Erlebnissen zu stellen und mit den Strafverfolgungsbehörden zu kooperieren. Ohne sie wäre eine Verfolgung dieser Straftaten unmöglich. Der Rechtsstaat ist ohne die Kooperation der Gesellschaft nicht haltbar.

Um den Rechtstaat sollen wir uns alle sorgen. Denn der Rechtstaat ist eine unserer besten Errungenschaften. Er ist Grundvoraussetzung für Demokratie, da diese ohne einen Rechtstaat nicht funktionieren kann. Demokratie beruht grundsätzlich auf Vertrauen. Vertrauen in ordentliche und faire Wahlen, in die Arbeit von Politikern, in die Gewaltenteilung und auf die Stabilität des Staates. Am Beispiel des Iraks kann wird der Zusammenhang sichtbar. Die Versuche eine stabile Demokratie im Irak zu etablieren sind gescheitert. Korruption und Willkür, die Abwesenheit des Rechtsstaats, lies kein Vertrauen in den Staat entstehen. Es führte zu inneren Kämpfen und letztlich zum Aufstieg des IS.

Die Übergriffe in Köln sollten keine Frage der Flüchtlingspolitik sein. Sie sind Straftaten die aufgeklärt und geahndet werden müssen. Mit den bestehenden Gesetzen und Regelungen. Lassen wir den Rechtsstaat seine Aufgabe erfüllen und zeigen wir seine Vorteile. Vielleicht übernimmt das ein oder andere Land den Rechtsstaat als Teil des attraktiven europäischen Lebensstiles und schafft den Sprung zu einer gerechteren Zukunft.
