---
author: maximilian-roell
title: "Passen Rosenmontagsumzüge und die Fastenzeit zusammen?"
published_at: 2016-02-13
tags: ['Gesellschaft', 'Brauchtum', 'Fastenzeit']
teaser_text: >
    Die Narren weinten am Rosenmontag, egal, ob sie es Karneval oder Fastnacht nannten. An vielen Orten wurden die Rosenmontagsumzüge wegen Sturmwarnungen abgesagt. Einige Festkomitees ließen nur verkleinerte Umzüge zu, in Köln verzichtete man etwa auf Pferde. Damit könnte die Geschichte eigentlich vorbei sein, wie der Karneval am Aschermittwoch vorbei ist. Doch viele Jecken wollen sich den Zug nicht nehmen lassen. Also wollen sie ihn, etwa in Trier, nachholen. Doch damit zerreißen sie die Einheit im liturgischen und karnevalistischen Jahreskreis, nach dem das Land tickt.
---

Die Narren weinten am Rosenmontag, egal, ob sie es Karneval oder Fastnacht nannten. An vielen Orten wurden die Rosenmontagsumzüge wegen Sturmwarnungen abgesagt. Einige Festkomitees ließen nur verkleinerte Umzüge zu, in Köln verzichtete man etwa auf Pferde. Damit könnte die Geschichte eigentlich vorbei sein, wie der Karneval am Aschermittwoch vorbei ist. Doch viele Jecken wollen sich den Zug nicht nehmen lassen. Also wollen sie ihn, etwa in Trier, nachholen. Doch damit zerreißen sie die Einheit im liturgischen und karnevalistischen Jahreskreis, nach dem das Land tickt.

Die Bischöfe, die darüber wachen, haben dazu keine einhellige Meinung. In Trier segnete Ackermann prophylaktisch die Entscheidungen des Komitees ab, weil man ja auch in der Fastenzeit fröhlich sein könne. Kardinal Woelki versuchte, den Zug wenigstens noch auf Laetare zu lenken, wurde aber vom Einzelhandel überstimmt, der an einem verkaufsoffenen Sonntag keinen Karnevalszug möchte. Manche Beobachter mag denken: Ist die Kirche närrisch?

### Feiern und Bützen geht auch ohne Kirche
Dabei können sich die Karnevalisten einen Kommentar der Bischöfe zu ihrem Umzug ganz allgemein verbitten. Denn Kirche und Fastnacht haben in der säkularisierten Gesellschaft wenig miteinander zu tun. Das Feiern und Bützen ist nur noch für die wenigsten das letzte Aufgebot vor dem Fasten, sondern eine gerne angenommene Gelegenheit, anerkannt die Sau raus zu lassen. Wenn es im säkularisierten Karnevalsvergnügen keine direkte Verbindung mehr zur Praxis der Fastenzeit gibt, dann können die Narren auch in der Fastenzeit den Rosenmontagsumzug nachholen und die Bischöfe in der Kirche sein lassen.

### Die Kirche will lieber populär sein
Zudem schadet es den Bischöfen in der öffentlichen Wahrnehmung nicht, wenn sie den zweiten Versuch der Karnevalisten absegnen. So wirken sie nicht wie verknöcherte Moralapostel, die streng auf die Einhaltung des Kirchenjahres pochen, sondern wie flexible, mitfühlende Fans karnevalistischer Fröhlichkeit, die auch in der Fastenzeit geht. Schlechte Presse wird Ackermann nicht wegen seinem Placet bekommen. Und Woelki kann sich eines konstruktiven Vorschlages rühmen, der die Interessen der Karnevalisten berücksichtigt.

### Religion und Regeln
Andererseits ist die Verbindung von Fastnacht und Kirche noch vergleichsweise eng. Das Fest hängt immer noch vom Osterkalender ab und wird durch die kirchliche Tradition klar begrenzt. Die Jecken profitieren von dieser religiösen Grundierung ihrer Feier. Sie können sich ganz auf die Entfaltung konzentrieren, weil ihnen der Rahmen vorgegeben ist. Das verpflichtet auch zur Einhaltung von Regeln. Dazu gehört wesentlich: An Aschermittwoch ist Schluss. Ein Rosenmontagszug in der Fastenzeit ist fernab von dem, was die Kirche unter Karneval versteht. Es geht um Besinnung, Buße, und Umkehr. Das wird beim Karneval verfehlt. Aus religiöser Sicht ist daher der zweite Versuch abzulehnen.

### Leitkultur bedeutet Verpflichtung

Durch seine Verwurzelung und seine religiöse Dimension ist der Karneval nicht nur Träger von Gaudi, sondern auch von Identität. In der Flüchtlingsdebatte ist das deutlich geworden. Flüchtlinge müssen über die Grenzen und Möglichkeiten des Karnevals aufgeklärt werden. Nicht nur, um Übergriffe zu vermeiden, sondern auch, um Integration zu ermöglichen. Wer den Karneval nicht versteht, der versteht auch einen wichtigen Teil Deutschlands nicht. Reiner Mohr spricht im Deutschlandradio vom Karneval, der „zum eisernen Kernbestand der deutschen Leitkultur gehört.“ Durch die enge Verflechtung zwischen Religion und Fastnacht-Tradition, hat die Kirche die Aufgabe, nicht nur die auf die religiöse, sondern auch auf die kulturelle Dimension hinzuweisen.

### Ausnahmen müssen die Regel bestätigen
Es sprechen gute Argumente dafür, dass die Bischöfe einen Umzug in der Fastenzeit absegnen. Das Fest ist säkularisiert und die Kirche hat in den Komitees nichts zu melden. Gleichzeitig können sich die Bischöfe als verständnisvolle Hirten präsentieren. Andererseits ist es auch ihre Aufgabe, auf die Einhaltung der Ordnung im Jahreskreis zu achten und Karneval gehört indirekt dazu, gerade im katholischen Rheinland, wo sich Religion und Brauchtum vermischen. Es ist zudem Teil der deutschen Kultur geworden, in der die Kirche nach wie vor eine wichtige Rolle einnimmt. Die Bischöfe tendieren dazu, lieber populär zu sein, als harte Entscheidungen zu treffen. Einmal mag das gehen, aber nur, wenn die Ausnahme die Regel bestätigt.
