
---
author: lukas-ansorge
title: "„Nach der Vorabendmesse bist du fällig“"
published_at: 2016-03-11
tags: ['Gesellschaft', 'Kirche', 'Moral', 'Werte']
teaser_text: >
    Der Fall des zurückgetreten Pfarrers Olivier Ndjimbi-Tshiende schlägt in der katholischen Kirchen hohe Wellen. Am vergangen Sonntag war der Kongolese mit deutscher Staatsangehörigkeit als Pfarrer der bayerischen Gemeinde Zorneding zurückgetreten.
---
Der Fall des zurückgetreten Pfarrers Olivier Ndjimbi-Tshiende schlägt in der katholischen Kirchen hohe Wellen. Am vergangen Sonntag war der Kongolese mit deutscher Staatsangehörigkeit als Pfarrer der bayerischen Gemeinde Zorneding zurückgetreten. Grund dafür waren anonyme Morddrohungen sowie rassistische Beleidigungen.

### Der Fall Olivier Ndjimbi-Tshiende

Im Herbst 2015 sprach die frühere CSU-Ortsvorsitzende von Zorneding Sylvia Boher im Parteiblatt *Zorneding Report* im Kontext der Flüchtlingskrise von einer „Invasion“ und äußerte die Angst, dass Bayern von Flüchtlingen überrannt werde. Nachdem Ndjimbi-Tshiende sowie der Pfarrgemeinderat seiner Pfarrei diese Aussagen kritisiert hatten, äußerte sich der Stellvertrteter Bohers Johann Haindl mit den Worten: „Der muss aufpassen, dass ihm der Brem (Zornedings Altpfarrer) nicht mit dem nackerten Arsch ins Gesicht springt, unserem Neger.“ Daraufhin mussten Boher und Haindl ihr Amt als Ortsvorsitzende der CSU niederlegen.

Für den Rücktritt ausschlaggebend waren nach eigenen Angaben anonyme Morddrohungen und rassistische Beleidigungen, die der Pfarrer seit November 2015 wiederholt erhielt. Inwiefern diese mit den Aussagen der beiden CSU-Politiker zusammenhängen ist ungewiss.

Ndjimbi-Tshiende blicke ohne Zorn auf seine Jahre in Zorneding zurück und habe sich mit Sylvia Boher versöhnt, wie das Erzbischöfliche Ordinariat mitteilte.

Der bayerische Ministerpräsident Horst Seehofer verurteile die Morddrohungen als inakzeptabel und forderte keine Toleranz zuzulassen. Gleichzeitig stritt er jede Verbindung zur Rolle der CSU ab. Über eine solche Verbindung wurde insbesondere in den sozialen Netzwerken spekuliert.

### Anonymität im Netz

Der Fall zeichnet sich durch die Anonymität der Morddrohungen aus. Unadressierte Briefe oder das Internet bieten sich für ein solches Vorgehen an. Eine neue E-Mail-Adresse oder ein Profil bei Facebook lässt sich einfach und unter falschem Namen erstellen, sodass eine Rückverfolgung schwierig wird. In diesen rechtlich schwer zu belangenden Räumen kommt es immer wieder zu Beleidigungen, Drohungen und Verleumdungen, die öffentlich keiner äußern würde.

Im Falle des Erzbischofs Schick veröffentlichen die Täter einen gefälschten Facebook-Eintrag, in dem er Pegidaanhängern mit Höllenfeuer drohte.

Der deutsche Staat gewährt jedem Bürger das Recht der Meinungsfreiheit, das auch Kritik einschließt. Diese Kritik ist jedoch in dem Maße zu äußern, dass die kritisierte Person nicht in ihrer Menschenwürde verletzt wird.

### Rassismus in Deutschland

In den vergangenen Jahren hat sich in der deutschen Öffentlichkeit ein erschreckender Zuwachs an Fremdenfeindlichkeit ereignet. Seit Ende 2014 gibt es die islamophobe Pegida und die Flüchtlingskrise des letzten Jahres hat zu einem Zulauf der Wähler zu politisch rechts orientierten Parteien geführt. Nicht umsonst konnte die AfD bei den hessischen Kommunalwahlen mit 11,9 % zur drittstärksten Partei aufsteigen.

Der Fremdenfeindlichkeit ist entgegenzuhalten, dass Integration gelingen kann. Ein Austausch der Kulturen ist für beide Seiten eine Bereicherung. Auch Olivier Ndjimbi-Tshiende wurde vom Großteil seiner Gemeine akzeptiert und unterstützt, was nicht zuletzt die über 3000 Teilnehmer der Kundgebung „Rassismus entgegentreten“, die am Mittwoch in Zorneding stattfand, zeigen.

Leider gelingt es einer Minderheit von fremdenfeindlichen Personen immer wieder, ihre ausländischen Mitmenschen durch Drohungen oder Gewalttaten einzuschüchtern. Oftmals bleibt den Betroffenen daraufhin aus Angst vor ihrem Leben nur der Umzug an einen anderen Ort, so auch im Falle Ndjimbi-Tshiendes.

### Was bleibt?

In Folge der Ereignisse sollte sich die katholisch Kirche erneut und kompromisslos gegen jegliche Form von Rassismus aussprechen. Auf keinen Fall darf es zu einem Totschweigen solcher oder ähnlicher Vorfälle kommen. Den Tätern muss aufgezeigt werden, dass der Großteil der Bevölkerung ihrem Handeln nicht zustimmt.

Noch nie war es so einfach eine andere Person zu diffamieren oder zu bedrohen. Die Anonymität des Internets ist Segen und Fluch zugleich. Jeder Nutzer sollte sich dessen bewusst sein und erhaltene Informationen, die andere Menschen in ein schlechtes Licht rücken, ein zweites Mal überprüfen.
