---
author: lukas-ansorge
title: "Nächstenliebe in der heutigen Zeit"
published_at: 2016-02-19
tags: ['Nächstenliebe']
teaser_text: >
    Papst Franziskus hat sich auf seiner Mexikoreise erneut mit der Nächstenliebe befasst. Während der Messe in Ecatepec zitierte er die Worte Paul VI., der 1970 in einer Radioansprache an die Mexikaner gesagt hatte: „Ein Christ darf nicht unterlassen, seine Solidarität zu beweisen, um die Situation derer zu lösen, zu denen das Brot der Kultur oder die Gelegenheit zu einer ehrenwerten Arbeit noch nicht gelangt sind“.
---

Papst Franziskus hat sich auf seiner Mexikoreise erneut mit der Nächstenliebe befasst. Während der Messe in Ecatepec zitierte er die Worte Paul VI., der 1970 in einer Radioansprache an die Mexikaner gesagt hatte: „Ein Christ darf nicht unterlassen, seine Solidarität zu beweisen, um die Situation derer zu lösen, zu denen das Brot der Kultur oder die Gelegenheit zu einer ehrenwerten Arbeit noch nicht gelangt sind“.

Franziskus fügte diesen Worten die Aufforderung hinzu, „die Situation der Notleidenden zu verbessern, und in jedem Menschen einen Bruder oder eine Schwester und in jedem Bruder oder jeder Schwester Christus zu sehen“.

Worte, die das christliche Handlungsverständnis beschreiben, doch blickt man auf das Verhalten der Christen in Deutschland fällt auf, dass viele sich nicht daran orientieren. Weder hat jede Gemeinde die  geforderte Flüchtlingsfamilie aufgenommen, noch setzt sich jeder Gläubige aktiv für die Rechte anderer ein.

### Wie weit muss Nächstenliebe gehen?

Mit Sicherheit ist das Ideal der bedingungslosen Nächstenliebe für jeden Christen und Menschen unerreichbar. Wir müssen den Nachbarn, der sonntags den Rasen mäht, nicht in unser Herz schließen. Ein gelebtes Christsein erwartet von uns nicht Unmögliches, aber ein bisschen Aktivität fordert es eben doch.

In Deutschland befinden wir uns in einer Situation, in der es sich als „passiver“ Christ gut leben lässt. Schließlich zahlen wir Kirchensteuern und spenden Geld, sodass andere unseren Part des aktiven Diensts am Nächsten ausführen können. Die Frage, ob das ausreicht, muss jeder Gläubige mit sich ausmachen. Nicht umsonst ist der Glaube einer der intimsten Werte, die den Menschen charakterisieren.

### Ausnahmesituation Flüchtlingskrise

Bis ins vergangene Jahr hinein konnten wir uns mit der Ausrede behelfen: „Ich würde ja helfen, aber ich weiß nicht wo und wie.“ Niemand erwartet von uns, dass wir selbst in die Krisengebiete der Welt reisen, um vor Ort den Menschen beizustehen. Aber was tun wir, wenn die Hilfsbedürftigen plötzlich zu uns kommen? Unsere Beobachterrolle reicht nicht mehr aus. Wir müssen selbst aktiv werden, um das Leid in unserem direkten Umfeld zu mindern.

### Glaubensvorbild Jesus Christus

Blicken wir in das Neue Testament, ist die Sache klar.  Wir alle sind von Jesus persönlich angesprochen und dazu aufgefordert, unseren Mitmenschen beizustehen. Im Doppelgebot der Liebe trägt er uns auf unseren Nächsten zu lieben wie uns selbst. Das Gleichnis des barmherzigen Samariters macht deutlich, dass die zu erwartende Hilfe nicht von der Konfession des Bedürftigen abhängen darf. Wir müssen dem Christen in gleicher Weise beistehen wie dem Muslim, dem Buddhisten oder dem Atheisten.

### Franziskus' Kirche der Armen

Seit Beginn seines Pontifikats wird Franziskus nicht müde, sein Prinzip der Kirche für die Armen zu verkünden und zu leben: Besuche in Slums und Elendsvierteln, spontane Treffen mit den Gläubigen auf dem Petersplatz. Nur so kann er sich ein Leben in der Nachfolge Jesu vorstellen. Wenn schon das Oberhaupt aller Katholiken sich nicht zu schade dazu ist, mit den Menschen in Kontakt zu treten und ihnen Hoffnung zu geben, um wieviel mehr sind dann wir zu Taten aufgerufen?

Die Resonanz in der Gesellschaft ist gut. Laut Umfragewerten ist er bei den Deutschen beliebter als sein Vorgänger Benedikt XVI.

### Realisierbarkeit von Nächstenliebe

Die Botschaft scheint klar zu sein. Nächstenliebe gehört essentiell zum Christsein dazu. Aber was ist mit den vielen „passiven“ Christen. Sicherlich ist ihr Christsein nicht weniger wert als das derjenigen, die sich engagieren. Eine aktiv gelebte Nächstenliebe im Sinne Franziskus erfordert Fähigkeiten, die nicht jeder Gläubige mitbringt. Manchen Christen fällt es schwer in offener Haltung auf Menschen zuzugehen und ihnen Hilfe anzubieten. Manche Christen haben so viele eigene Probleme, dass sie diejenigen der anderen nicht sehen. Wieder anderen fehlt es an der Zeit, sich aktiv in ihrem Umfeld zu engagieren. Diese und andere Argumente können ein Leben als „passiver“ Christ legitimieren, aber es sollte auch klar sein: Die pure Bequemlichkeit tut das nicht. Nächstenliebe ist und bleibt ein urchristliches Gut. Es geht nicht darum, es Ausnahmegestalten wie Jesus oder Franziskus gleichzutun und sich für die Armen aufzuopfern. Doch das Vorbild der Heiligen darf uns nicht egal sein. Wir sollten uns davon inspirieren lassen, und beginnen unser eigenes Leben zu reflektieren. Vielleicht gibt es doch noch die ein oder andere Möglichkeit, unseren Mitmenschen den Dienst der Nächstenliebe zu erweisen.
