---
display_name: Kerstin Barton
initials: kb
short_bio: >
    Geboren bin ich 1992 in Menden (Sauerland), wo ich bis heute lebe. Nach meinem Abitur am Heilig-Geist Gymnasium, studiere ich seit 2012 an der TU Dortmund. Neben meinem Hauptfach, angewandte Sprachwissenschaften, studiere ich noch außerdem noch Literatur- und Kulturwissenschaften, sowie Politik. 2015 absolvierte ich ein Auslandssemester in Krakau. In meiner Heimatgemeinde engagierte ich mich zunächst als Messdienerin, später als Messdienerleiterin und aktuell als Küsterin. Seit April 2016 gehöre ich zum Team von kath.de.
---
