---
display_name: Alexander Radej
initials: ar
short_bio: >
    Geboren bin ich in Frankfurt am Main und studiere seit 2012 Katholische Theologie und Geschichte. Im Jahre 2014 absolvierte ich ein Praktikum bei der Bahnhofsmission Mannheim und arbeitete dort ehrenamtlich bis 2015. Ich gehöre der Orthodoxen Kirche an. Meine Forschungsschwerpunkte sind im Besonderen die Katholischen Ostkirchen und die Orthodoxie. Ich bin seit August 2016 Stellvertretender Redaktionsleiter bei Kath.de.
---
