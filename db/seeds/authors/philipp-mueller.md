---
display_name: Philipp Müller
initials: pm
short_bio: >
  Ich komme aus der Nähe von Fulda und engagiere mich dort in Pfarrei und Bistum. Nach dem Abitur und einem Freiwilligen Jahr besuchte ich 2012/2013 die Emmanuel School of Mission in Rom und begann danach ein Philosophiestudium an der Philosophisch-Theologischen Hochschule Sankt Georgen in Frankfurt am Main. In meiner Freizeit arbeite ich mit Veranstaltungstechnik und in der Videoproduktion.
---
Ich komme aus der Nähe von Fulda und engagiere mich dort in Pfarrei und Bistum. Nach dem Abitur und einem Freiwilligen Jahr besuchte ich 2012/2013 die Emmanuel School of Mission in Rom und begann danach ein Philosophiestudium an der Philosophisch-Theologischen Hochschule Sankt Georgen in Frankfurt am Main. In meiner Freizeit arbeite ich mit Veranstaltungstechnik und in der Videoproduktion.

Bei kath.de schreibe ich Texte.
