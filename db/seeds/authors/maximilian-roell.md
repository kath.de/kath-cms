---
display_name: Maximilian Röll
initials: mr
short_bio: >
    Geboren in Frankfurt am Main wohne ich in der Nähe von Limburg. Nach meinem Abitur habe ich zwischen 2007 bis 2012 Geschichte, Katholische Theologie und Klassische Archäologie an der Goethe-Universität in Frankfurt studiert. Seit 2013 promoviere ich in der Geschichtswissenschaft zu den Themen Katholizismusforschung, Ideen- und Mediengeschichte. Dazu nehme ich am Medienstudium in St. Georgen teil. Bei kath.de arbeite ich seit Januar 2014 als Freier Mitarbeiter und seit Oktober 2015 als Chefredakteur.
---
