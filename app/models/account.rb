# frozen_string_literal: true

class Account < ApplicationRecord
  validates :username, presence: true

  attribute :settings, :jsonb, default: {}

  def self.from_cas(cas)
    find_by(username: cas['user']) || UnprivilegedAccount.new(cas)
  end

  def authorized?
    true
  end

  def default_author
    Author.payload_where(email: settings[:default_author] || username).first
  end

  class UnprivilegedAccount
    include ActiveModel::Model

    attr_accessor :username, :extra_data

    def initialize(cas)
      self.username = cas['user']
      self.extra_data = cas['extra_data'] || {}
    end

    def authorized?
      false
    end

    def default_author
      nil
    end
  end
end
