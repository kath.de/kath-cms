module EntryWithPaperTrail
  def self.prepended(klass)
    klass.has_paper_trail(
      versions: {
        class_name: 'Version'
      },
      skip: [:payload],
      meta: {
        entry_type: :model_name
      }
      )
  end
end

class Version < PaperTrail::Version
  belongs_to :whodunnit, class_name: 'Account', foreign_key: :whodunnit

  scope :recent_changes, -> { order(created_at: :desc) }
end
