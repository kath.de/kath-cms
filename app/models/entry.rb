
# frozen_string_literal: true
# rubocop:disable Metrics/LineLength
# SELECT  "entries".* FROM "entries" WHERE "entries"."type" IN ('Kommentar') AND (payload->'text')::text LIKE '%Kritikpunkt%';

#  SELECT name, payload->'title' AS "title" FROM entries WHERE type='Kommentar' AND author_ids @> ARRAY(SELECT id FROM entries WHERE type = 'Author' AND name = 'philipp-mueller');

# SELECT kommentar.name, kommentar.author_ids, author.name AS author_name FROM entries AS kommentar, (SELECT author.id, author.name FROM entries AS author WHERE author.type='Author') AS author WHERE kommentar.type='Kommentar' AND kommentar.author_ids @> ARRAY[author.id];
# SELECT kommentar.name, kommentar.author_ids, array_agg(author.name) AS author_name FROM entries AS kommentar, (SELECT author.id, author.name FROM entries AS author WHERE author.type='Author') AS author WHERE kommentar.type='Kommentar' AND kommentar.author_ids @> ARRAY[author.id] GROUP BY kommentar.id;

# SELECT kommentar.name, kommentar.author_ids, array_agg(author) AS authors FROM entries AS kommentar JOIN entries AS author ON kommentar.author_ids @> ARRAY[author.id] WHERE author.type='Author' AND kommentar.type='Kommentar' GROUP BY kommentar.id;
# SELECT kommentar.name, kommentar.author_ids, array_agg(row_to_json(author)) AS authors FROM entries AS kommentar JOIN entries AS author ON kommentar.author_ids @> ARRAY[author.id] WHERE author.type='Author' AND kommentar.type='Kommentar' GROUP BY kommentar.id;
