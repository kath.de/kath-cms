# frozen_string_literal: true

class ParseJson
  attr_reader :input

  def initialize(input)
    @input = input
  end

  def call
    if input.blank?
      nil
    elsif input.respond_to? :keys
      input
    else
      begin
        JSON.parse(input)
      rescue JSON::ParserError => e
        raise e
      end
    end
  end
end
