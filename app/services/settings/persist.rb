# frozen_string_literal: true

class Settings::Persist < Entry::Persist
  def prepare_data
    super

    parse_settings_yaml
  end

  def parse_settings_yaml
    data[:settings] = YAML.safe_load(data[:settings], [Date]) if data.key? :settings
  end

  def generate_slug; end

  def after_save
    super

    begin
      Faraday.get Rails.application.config.preview_base_url + "/settings/reload"
    rescue Exception => e
      @flash[:warning] = ['settings.could_not_reload', error: e.message, scope: '']
    end
  end
end
