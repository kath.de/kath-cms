# frozen_string_literal: true

class Tagesreport::New < Entry::New
  def set_default_values
    super

    resource.date ||= Time.zone.today
    resource.display_title ||= resource.default_display_title

    resource.entries ||= Tagesreport.default_entries_content(resource.day_of_week)
  end
end
