# frozen_string_literal: true

class Tagesreport::Persist < Entry::Persist
  def prepare_data
    clean_empty_slug

    parse_entries_yaml(data[:entries]) if data.key? :entries
  end

  def parse_entries_yaml(yaml)
    # TODO: Validate schema?
    # TODO: Extract job
    begin
      entries = Tagesreport.parse_entries_hash(yaml)
    rescue Psych::SyntaxError => e
      # Don't proceed if invalid YAML
      @flash[:warning] = ['yaml.syntax_error', error: e.message, scope: '']
      return
    end

    all_entries = entries.values.flatten.compact.map do |entry|
      außerdem = entry['außerdem']
      if außerdem
        [entry] + außerdem.compact
      else
        entry
      end
    end.flatten

    all_urls = all_entries.flatten.compact.map { |e| e['url'] }

    publishers = Publisher.by_url all_urls

    all_urls.each do |url|
      next if !url || publishers.has_key?(Publisher.normalize_url(url))
      service = Metadata::Collect.call(url)

      unless service.success?
        Rails.logger.error "Failed to retrieve metadata for url #{url}"
      end
    end
  end

  def generate_slug
    # always set slug from date
    resource.slug = resource.generate_slug
  end
end
