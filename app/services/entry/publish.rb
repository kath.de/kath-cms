# frozen_string_literal: true

class Entry::Publish < Service::Resource
  attr_reader :status

  def call
    if resource.published?
      @status = :already_published
    elsif resource.publish!
      @status = :success
    else
      @status = :error
      return fail! resource.errors
    end

    self
  end

  def no_change?
    status == :already_published
  end
end
