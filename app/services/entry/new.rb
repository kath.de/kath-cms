# frozen_string_literal: true

class Entry::New < Service::Resource
  def initialize(resource_class)
    @resource_class = resource_class
    super resource_class.new
  end

  def call
    set_default_values

    self
  end

  def set_default_values; end
end
