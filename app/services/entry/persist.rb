# frozen_string_literal: true

class Entry::Persist < Service::Persist
  def prepare_data
    parse_payload_json

    clean_empty_slug

    normalize_linefeed
  end

  def update_resource
    generate_slug
  end

  private

  def parse_payload_json
    data[:payload] = parse_json(data[:payload]) if data.key? :payload
  end

  def parse_json(payload)
    json_parser.new(payload).call
  end

  def json_parser
    ParseJson
  end

  def clean_empty_slug
    if data.key?(:slug) && data[:slug].empty?
      # do not set slug to an empty value
      data.delete :slug
      @slug_has_been_cleaned = true
    else
      @slug_has_been_cleaned = false
    end
  end

  def generate_slug
    return unless @slug_has_been_cleaned || data.key?(:slug)
    # automatically generate slug if resource provides generator and slug has not been set

    resource.slug = resource.generate_slug if resource.slug.blank?
  end

  def normalize_linefeed
    data.each do |key, value|
      if value.respond_to? :gsub
        data[key] = value.gsub("\r\n", "\n")
      end
    end
  end
end
