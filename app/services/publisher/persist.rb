# frozen_string_literal: true

class Publisher::Persist < Entry::Persist
  def prepare_data
    clean_empty_slug
  end

  def update_resource
    normalize_domain

    generate_slug
    generate_title
  end

  def normalize_domain
    resource.domain = Publisher.normalize_domain(resource.domain)
  end

  def generate_title
    resource.title ||= Publisher.normalize_title(resource.domain)
  end
end
