# frozen_string_literal: true

require 'publisher/persist'

class Metadata::CreatePublisher < Service::Resource
  attr_reader :metadata

  def initialize(metadata)
    super(nil)
    @metadata = metadata
    @is_new = false
  end

  def call
    @resource = Publisher.by_url(metadata.url) || begin
      @is_new = true
      domain = Addressable::URI.parse(metadata.url).host
      Publisher::Persist.call(Publisher, domain: domain, title: metadata.publisher).resource
    end

    self
  end

  def new?
    @is_new
  end
end
