# frozen_string_literal: true

require 'metadata/builder'
require 'improved_meta_inspector'

class Metadata
  class Collect < Service::Base
    attr_reader :url, :metadata, :publisher, :publisher_new

    def initialize(url)
      @url = url
      @metadata = nil
    end

    def call
      if @url.nil?
        return fail! "Missing URL"
      end

      begin
        metainspector = create_metainspector
      rescue MetaInspector::ParserError => error
        return fail! error
      end

      @metadata = Metadata::Builder.new(metainspector).call

      publisher_service = Metadata::CreatePublisher.call(@metadata)
      @publisher = publisher_service.resource
      @publisher_new = publisher_service.new?

      self
    end

    private

    def create_metainspector
      @metainspector ||= ::ImprovedMetaInspector.new(url)
    end
  end
end
