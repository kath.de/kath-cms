# frozen_string_literal: true

class Metadata::Builder
  def initialize(metainspector)
    @metainspector = metainspector
  end

  def call
    data = {
      url: url,
      title: title,
      subtitle: subtitle,
      provider: provider,
      published_at: published_at,
      tags: tags,
      authors: authors,
      publisher: publisher,
    }
    Metadata.new data
  end

  protected

  attr_reader :metainspector

  def url
    metainspector.canonical_url
  end

  def title
    metainspector.best_title_stripped
  end

  def subtitle
    metainspector.first_sentence_of_description
  end

  def original_url
    metainspector.url
  end

  def provider
    metainspector.host
  end

  def published_at
    metainspector.publication_date || Time.zone.now
  end

  def tags
    []
  end

  def publisher
    metainspector.provider_names.first
  end

  def authors
    metainspector.authors
  end
end
