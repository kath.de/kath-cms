# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action do
    @breadcrumbs = []
  end

  def show
    render locals: {
      recent_changes: Version.recent_changes.take(12)
    }
  end
end
