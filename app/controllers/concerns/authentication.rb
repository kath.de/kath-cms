# frozen_string_literal: true

require 'active_support/concern'

module Authentication
  extend ActiveSupport::Concern

  included do
    before_action :require_login

    helper_method :current_user, :signed_in?, :signed_out?, :authorized?
  end

  private

  def require_login
    return true if authorized?

    # User is signed in but not authorized
    if signed_in?
      render 'application/forbidden', status: :forbidden
      return false
    end

    head :unauthorized
    false
  end

  def current_user
    @current_user ||= Account.from_cas(session['cas']) if session['cas']
  end

  def signed_out?
    !current_user
  end

  def signed_in?
    !signed_out?
  end

  def authorized?
    signed_in? && current_user.authorized?
  end
end
