# frozen_string_literal: true

# @see https://szeryf.wordpress.com/2008/06/13/easy-and-flexible-breadcrumbs-for-rails/
require 'active_support/concern'

module Breadcrumbs
  extend ActiveSupport::Concern

  class Element
    include ActiveModel::Model

    attr_accessor :name, :url, :options
  end

  included do
    def breadcrumbs
      @breadcrumbs ||= []
    end

    def add_breadcrumb(name, url = nil, options = {})
      breadcrumbs << Breadcrumbs::Element.new(name: name, url: url, options: options)
    end
  end

  class_methods do
    protected

    def add_breadcrumb(name, path = nil, filter_options = {})
      element_options = filter_options.delete(:options) || {}

      before_action(filter_options) do |controller|
        path = path.call if path.respond_to? :call
        controller.send(:add_breadcrumb, name, path, element_options)
      end
    end
  end
end
