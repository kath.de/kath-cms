require 'active_support/concern'

module ResourceBreadcrumbs
  extend ActiveSupport::Concern

  included do
    before_action do
      name = params[:action] != 'index' ? url_for([namespace, resource_class]) : nil
      add_breadcrumb display_resource_name(resource_name), name
    end

    before_action(only: %i[edit show]) do
      name = params[:action] == 'edit' ? url_for([namespace, requested_resource]) : nil
      add_breadcrumb dashboard.display_resource(requested_resource), name
    end
  end
end
