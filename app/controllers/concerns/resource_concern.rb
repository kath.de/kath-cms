module ResourceConcern
  extend ActiveSupport::Concern

  # overwrites from administrate/application_controller
  def translate_with_resource(key, resource = nil, options = {})
    model_name = if resource.nil?
                   # re-implements behaviour of administrate/application_controller
                   resource_class.model_name
                 else
                   model_name(resource)
                 end
    resource_param = model_name.param_key.underscore
    options = options.dup.reverse_merge(
      default: [key.to_sym],
      resource: model_name.human,
      scope: 'administrate.controller',
    )

    t("#{key}_#{resource_param}", options)
  end

  def model_name(resource)
    resource = model_for_resource(resource) unless resource.respond_to? :model_name
    resource.model_name
  end

  def model_for_resource(resource)
    resource.to_s.classify.constantize
  end

  # from Administrate::ApplicationHelper
  def display_resource_name(resource_name)
    resource_name
      .to_s
      .classify
      .constantize
      .model_name
      .human(
        count: 0,
        default: resource_name.to_s.pluralize.titleize,
      )
  end
end
