module ControllerBase
  extend ActiveSupport::Concern

  include Authentication
  include Breadcrumbs

  included do
    protect_from_forgery with: :exception, unless: -> { request.format.json? }, prepend: true

    before_action do
      add_breadcrumb 'Dashboard', root_path
    end

    # before_action :set_paper_trail_whodunnit
  end

  def user_for_paper_trail
    current_user
  end
end
