# frozen_string_literal: true

module Admin
  class PublishController < ApplicationController
    def create
      result = Entry::Publish.new(resource).call

      message = t("admin.controller.publish.#{result.status}_html",
                  title: resource_title(resource), date: l(resource.published_at))

      unless result.success? || result.no_change?
        message = render_to_string partial: 'admin/application/error_notice',
                                   locals: {
                                     errors: resource.errors.full_messages,
                                     message: message,
                                   }
      end

      redirect_to(
        [namespace, resource],
        notice: message,
      )
    end

    def destroy
      result = if resource.unpublished?
                 :already_unpublished
               else
                 resource.unpublish! ? :success : :failure
               end

      message = t("admin.controller.unpublish.#{result}_html", title: resource_title(resource))

      if result == :failure
        message = render_to_string 'admin/application/error_notice',
                                   errors: resource.errors.full_messages,
                                   message: message
      end

      redirect_to(
        [namespace, resource],
        notice: message,
      )
    end

    def resource
      @resource ||= Entry.find(params[:id])
    end

    protected

    def resource_title(resource)
      (resource.try(:title) || resource.slug).presence || 'Ø'
    end
  end
end
