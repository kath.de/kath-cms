# frozen_string_literal: true

require 'search/search'

module Admin
  class EntriesController < Admin::ApplicationController
    include ResourceBreadcrumbs
    include ResourceConcern

    before_action :set_paper_trail_whodunnit

    delegate :resource_class, :resource_name, :namespace, to: :resource_resolver
    helper_method :namespace
    helper_method :resource_name

    def index
      search_term = params[:search].to_s.strip
      resources = Search::Search.new(resource_resolver.resource_class, search_term).results

      params[:order] ||= "created_at"
      params[:direction] ||= "desc"

      order = Search::Order.new(params[:order], params[:direction])
      resources = order.apply(resources)
      resources = resources.page(params[:page]).per(records_per_page)
      page = Administrate::Page::Collection.new(dashboard, order: order)

      render locals: {
        resources: resources,
        search_term: search_term,
        page: page,
        show_search_bar: show_search_bar?,
      }
    end

    def create
      result = create_service_class.call(resource_class, resource_params)

      resource = result.resource

      result.flash.each do |key, message|
        message = translate_with_resource message[0], resource, message[1] if message.is_a?(Array)
        flash[key] = message
      end

      if result.success?
        redirect_to(
          [namespace, resource],
          notice: translate_with_resource('create.success'),
        )
      else
        render :new, locals: {
          page: Administrate::Page::Form.new(dashboard, resource),
        }
      end
    end

    def new
      resource = create_service_class('New').call(resource_class).resource

      if current_user.default_author
        resource.authors = [current_user.default_author] if dashboard.attribute_types.key? :authors
      end

      render locals: {
        page: Administrate::Page::Form.new(dashboard, resource),
      }
    end

    def update
      result = updating_service.call(requested_resource, resource_params)

      result.flash.each do |key, message|
        message = translate_with_resource message[0], requested_resource, message[1] if message.is_a?(Array)
        flash[key] = message
      end

      if result.success?
        redirect_to(
          [namespace, requested_resource],
          notice: translate_with_resource('update.success'),
        )
      else
        render :edit, locals: {
          page: Administrate::Page::Form.new(dashboard, requested_resource),
        }
      end
    end

    # def params
    #  super.permit!
    # end

    private

    def updating_service
      create_service_class
    end

    def create_service_class(action = 'Persist')
      if resource_class.const_defined?(action)
        "#{resource_class.name}::#{action}"
      else
        "Entry::#{action}"
      end.constantize
    end
  end
end
