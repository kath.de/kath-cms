# frozen_string_literal: true

class DocsController < ApplicationController
  layout 'application'

  def show
    page = Page.by_slug(params[:path]).first!

    add_breadcrumb 'Docs', docs_path
    add_breadcrumb page.title

    render locals: {
      page: page,
    }
  end

  def index
    pages = Page.hidden

    add_breadcrumb 'Docs'

    render locals: {
      pages: pages,
    }
  end
end
