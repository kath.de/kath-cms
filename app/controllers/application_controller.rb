# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include ControllerBase

  def resource_resolver
    @_resource_resolver ||=
      Administrate::ResourceResolver.new('admin/entries')
  end

  helper_method :namespace
  def namespace
    'admin'
  end

  helper_method :nav_link_state
  def nav_link_state(_resource)
    :inactive
  end
end
