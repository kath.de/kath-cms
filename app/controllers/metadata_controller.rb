# frozen_string_literal: true

class MetadataController < ApplicationController
  skip_before_action :require_login

  def create
    url = params[:url]

    service = Metadata::Collect.call(url)

    if service.success?
      metadata = service.metadata

      rendered_yaml = render_to_string formats: :yaml, layout: false,
                                       content_type: 'text/yaml',
                                       locals: { metadata: metadata }

      render json: {
        status: :success,
        url: url,
        metadata: metadata,
        yaml: rendered_yaml,
        publisher: service.publisher,
        publisher_new: service.publisher_new,
      }
    else
      render json: { status: :error, error: service.error }
    end
  end
end
