# frozen_string_literal: true

module Admin::ApplicationHelper
  include Administrate::ApplicationHelper
  include ::ApplicationHelper
end
