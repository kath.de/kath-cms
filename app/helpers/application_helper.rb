# frozen_string_literal: true

module ApplicationHelper
  include Administrate::ApplicationHelper

  def action_link(resource, action)
    # byebug
    resource = model_name(resource).param_key.underscore if resource.is_a? Class
    link_to(translate_with_resource(action, resource, scope: 'administrate.actions'),
            url_for([action, namespace, resource]),
            class: 'button')
  end

  def translate_with_resource(key, resource, options = {})
    model_name = model_name(resource)
    resource_param = model_name.param_key.underscore
    options = options.dup.reverse_merge(default: [key.to_sym], resource: model_name.human)

    t("#{key}_#{resource_param}", options)
  end

  def resource_index_path(resource)
    path = [namespace, resource]

    path << :index if ActiveModel::Naming.uncountable? model_for_resource(resource)

    path
  end

  def logout_path
    '/logout'
  end

  def markdown(text)
    Kramdown::Document.new(text, input: 'markdown', header_offset: 1).to_html.html_safe
  end

  def preview_url(resource)
    if resource.is_a?(Kathde::Models::Concerns::PreviewToken)
      "#{public_url(resource)}?preview_token=#{resource.preview_token}"
    else
      raise "Cant generate preview url for #{model_name(resource)}"
    end
  end

  def public_url(resource)
    case resource
    when Kommentar
      Rails.application.config.preview_base_url + "kommentar/#{resource.slug}"
    when Wochenrueckblick
      Rails.application.config.preview_base_url + "wochenrueckblick/#{resource.slug}"
    when Tagesreport
      Rails.application.config.preview_base_url + "archiv/#{resource.date.to_s('Y/m/d')}"
    else
      raise "Cant generate public url for #{model_name(resource)}"
    end
  end

  private

  def model_name(resource)
    resource = model_for_resource(resource) unless resource.respond_to? :model_name
    resource.model_name
  end

  def model_for_resource(resource)
    resource.to_s.classify.constantize
  end
end
