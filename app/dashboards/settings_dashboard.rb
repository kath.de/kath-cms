# frozen_string_literal: true

class SettingsDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    settings:  YamlTextArea.with_options(min_rows: 50, uid_fields: [:slug]),
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    slug
    created_at
    updated_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    slug
    created_at
    updated_at
    settings
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :settings,
  ].freeze

  LINK_ATTRIBUTES = %i[id slug].freeze

  def display_resource(entry)
    # need to overwrite, because `#title` could be a settings key
    entry.slug
  end
end
