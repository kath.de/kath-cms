# frozen_string_literal: true

class TagesreportDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    display_title: Field::String,
    cover: Field::BelongsTo.with_options(class_name: "Image"),
    aufmacher_title: Field::String,
    aufmacher_body:  MarkdownTextArea.with_options(toolbar: :simple),
    authors: AuthorField,
    date: DateTimeField.with_options(format: :date),
    entries:  YamlTextArea.with_options(min_rows: 50, uid_fields: [:date]),
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    date
    status
    authors
    published_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    display_title
    date
    cover
    aufmacher_title
    aufmacher_body
    authors
    status
    published_at
    created_at
    updated_at
    entries
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    display_title
    date
    authors
    cover
    aufmacher_title
    aufmacher_body
    entries
  ].freeze

  LINK_ATTRIBUTES = %i[id date].freeze

  def filter_suggestions(page, current_user)
    filter_suggestions_with_author(page, current_user)
  end

  # Overwrite this method to customize how entries are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(entry)
    entry.date.to_s
  end
end
