# frozen_string_literal: true

class KommentarDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    title: Field::String,
    slug:  Field::String,
    vgwort_counter: Field::String,
    cover: Field::BelongsTo.with_options(class_name: "Image"),
    authors: AuthorField,
    body:  MarkdownTextArea,
    teaser_text:  MarkdownTextArea.with_options(toolbar: :simple),
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    title
    status
    authors
    published_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    title
    slug
    authors
    cover
    created_at
    updated_at
    status
    published_at
    vgwort_counter
    teaser_text
    body
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    title
    slug
    published_at
    vgwort_counter
    authors
    cover
    teaser_text
    body
  ].freeze

  LINK_ATTRIBUTES = %i[id title].freeze

  def filter_suggestions(page, current_user)
    filter_suggestions_with_author(page, current_user)
  end
end
