# frozen_string_literal: true

require 'administrate/base_dashboard'

class EntryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {

    id: Field::Number,

    slug: Field::String,

    type: Field::String,

    payload: JsonField,

    created_at: DateTimeField,

    updated_at: DateTimeField,

    published_at: PublishedAtField,

    status: StatusField,

  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :slug,
    :type,
    :status,
    :created_at,
    :updated_at,
    #:payload,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    id
    slug
    type
    status
    published_at
    created_at
    updated_at
    payload
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    slug
    type
    payload
    published_at
  ].freeze

  LINK_ATTRIBUTES = %i[id slug].freeze

  FILTER_SUGGESTIONS = [
    'is:published', 'is:draft',
  ].freeze

  # Overwrite this method to customize how entries are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(entry)
    entry.try(:title) || entry.slug
  end

  def filter_suggestions(_page, _current_user)
    self.class::FILTER_SUGGESTIONS
  end

  def filter_suggestions_with_author(_page, current_user)
    author_name = current_user.default_author.try(:slug) || '*'

    self.class::FILTER_SUGGESTIONS + [
      "author:#{author_name}",
    ]
  end

  def self.for(resource)
    Object.const_get(resource.model_name.name + "Dashboard").new
  end
end
