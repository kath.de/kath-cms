# frozen_string_literal: true

class ImageDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    image: Field::ActiveStorage.with_options(
      index_display_preview: true,
      index_display_count: false,
      index_preview_size: [100, 100],
      show_preview_size: [400, 0]
    ),
    slug: Field::String,
    caption: Field::String,
    cite: Field::String,
    description: Field::String,
    entries: Field::HasMany.with_options(foreign_key: "cover_id"),
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    image
    caption
    entries
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    image
    slug
    caption
    description
    cite
    created_at
    updated_at
    entries
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    image
    slug
    caption
    cite
    description
  ].freeze

  LINK_ATTRIBUTES = %i[id caption].freeze

  def filter_suggestions(page, current_user)
    filter_suggestions_with_author(page, current_user)
  end
end
