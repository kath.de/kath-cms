# frozen_string_literal: true

class PageDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    title: Field::String,
    long_title: Field::String,
    subtitle: Field::String,
    body:  MarkdownTextArea,
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    title
    slug
    status
    published_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    title
    long_title
    subtitle
    slug
    status
    published_at
    created_at
    updated_at
    body
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    slug
    title
    long_title
    subtitle
    published_at
    body
  ].freeze

  LINK_ATTRIBUTES = %i[id title].freeze
end
