# frozen_string_literal: true

class AuthorDashboard < EntryDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = EntryDashboard::ATTRIBUTE_TYPES.merge({
    display_name: Field::String,
    short_bio:  MarkdownTextArea,
    body:  MarkdownTextArea,
    cover: Field::BelongsTo.with_options(class_name: "Image"),
    initials: Field::String,
    email: Field::Email,
    slug: Field::String,
  }.freeze)

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    display_name
    slug
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    display_name
    initials
    slug
    cover
    email
    created_at
    updated_at
    short_bio
    body
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    display_name
    initials
    slug
    email
    cover
    short_bio
    body
  ].freeze

  LINK_ATTRIBUTES = [:display_name].freeze

  # Overwrite this method to customize how entries are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(resource)
    resource.display_name
  end
end
