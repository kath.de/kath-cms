# frozen_string_literal: true

require 'administrate/field/text'
require 'yaml'

class SimpleEditorField < Administrate::Field::Text
  DEFAULT_ROWS = 15

  def min_rows
    arr = [options.fetch(:min_rows, DEFAULT_ROWS)]
    arr << to_s.lines.count + 2 unless data.nil?
    arr.max
  end

  def uid_fields
    Array(options.fetch(:uid_fields, [:id]))
  end

  def uid_params
    Hash[uid_fields.map { |field| [field, resource.send(field)] }]
  end

  def data_uid(_object)
    params = uid_params.to_query
    params = "?#{params}" if params
    "#{resource.model_name.param_key}#{params}##{attribute}"
  end

  delegate :to_s, to: :data
end
