# frozen_string_literal: true

class DateTimeField < Administrate::Field::Base
  # datetime_picker uses moment.js
  # @see http://momentjs.com/docs/#/displaying/format/
  NAMED_FORMATS = {
    datetime: 'YYYY-MM-DD HH:mm',
    date: 'YYYY-MM-DD',
  }.freeze
  DEFAULT_FORMAT = :datetime

  def format
    format = options.fetch(:format, DEFAULT_FORMAT)
    NAMED_FORMATS.fetch(format, format)
  end
end
