# frozen_string_literal: true

require 'administrate/field/base'

class PublishedAtField < Administrate::Field::DateTime
end
