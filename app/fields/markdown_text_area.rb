# frozen_string_literal: true

class MarkdownTextArea < SimpleEditorField
  DEFAULT_TOOLBAR = :normal

  def toolbar
    options.fetch(:toolbar, DEFAULT_TOOLBAR)
  end
end
