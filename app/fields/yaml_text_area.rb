# frozen_string_literal: true

class YamlTextArea < SimpleEditorField
  def to_s
    if data.respond_to? :to_str
      # If this is a string or something that turns into a string, leave it as string
      data.to_str
    else
      data.to_yaml.gsub(/\A---\n/, '')
    end
  end
end
