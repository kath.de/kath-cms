# frozen_string_literal: true

require 'administrate/field/base'

class AuthorField < Administrate::Field::HasMany
  def resources
    data
  end
end
