# frozen_string_literal: true

require 'administrate/field/base'

class JsonField < Administrate::Field::Base
  def to_s
    return '' unless data
    begin
      JSON.generate(data)
    rescue JSON::GeneratorError
      data.inspect
    end
  end

  def truncate
    to_s[0..truncation_length]
  end

  def pretty_format
    return '' unless data
    begin
      JSON.pretty_generate(data)
    rescue JSON::GeneratorError
      data.inspect
    end
  end

  private

  def truncation_length
    options.fetch(:truncate, 50)
  end
end
