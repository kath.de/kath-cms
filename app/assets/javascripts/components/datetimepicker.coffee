$ () ->
  # datetime_picker uses moment.js
  # @see http://momentjs.com/docs/#/displaying/format/
  $("[data-date_time], .datetimepicker").each () ->
    $(this).datetimepicker({
      debug: false,
      format: this.getAttribute('data-date_time') || 'YYYY-MM-DD HH:mm'
    })
