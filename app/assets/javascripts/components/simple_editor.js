"use strict";
// adapted from https://github.com/sparksuite/simplemde-markdown-editor
//(function(){

// Some variables
var isMac = /Mac/.test(navigator.platform);

// Mapping of actions that can be bound to keyboard shortcuts or toolbar buttons
var bindings = {
};

var shortcuts = {
  "toggleFullScreen": "F11"
};

var getBindingName = function(f) {
  for(var key in bindings) {
    if(bindings[key] === f) {
      return key;
    }
  }
  return null;
};

var isMobile = function() {
  var check = false;
  (function(a) {
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

/**
 * Fix shortcut. Mac use Command, others use Ctrl.
 */
function fixShortcut(name) {
  if(isMac) {
    name = name.replace("Ctrl", "Cmd");
  } else {
    name = name.replace("Cmd", "Ctrl");
  }
  return name;
}

/**
 * Create icon element for toolbar.
 */
function createIcon(options, enableTooltips, shortcuts) {
  options = options || {};
  var el = document.createElement("a");
  enableTooltips = (enableTooltips == undefined) ? true : enableTooltips;

  if(options.title && enableTooltips) {
    el.title = createTootlip(options.title, options.action, shortcuts);

    if(isMac) {
      el.title = el.title.replace("Ctrl", "⌘");
      el.title = el.title.replace("Alt", "⌥");
    }
  }

  el.tabIndex = -1;
  el.className = options.className;
  return el;
}

function createSep() {
  var el = document.createElement("i");
  el.className = "separator";
  el.innerHTML = "|";
  return el;
}

function createTootlip(title, action, shortcuts) {
  var actionName;
  var tooltip = title;

  if(action) {
    actionName = getBindingName(action);
    if(shortcuts[actionName]) {
      tooltip += " (" + fixShortcut(shortcuts[actionName]) + ")";
    }
  }

  return tooltip;
}


// Saved overflow setting
var saved_overflow = "";

/**
 * Toggle full screen of the editor.
 */
function toggleFullScreen(editor) {
  // Set fullscreen
  var cm = editor.codemirror;
  cm.setOption("fullScreen", !cm.getOption("fullScreen"));


  // Prevent scrolling on body during fullscreen active
  if(cm.getOption("fullScreen")) {
    saved_overflow = document.body.style.overflow;
    document.body.style.overflow = "hidden";
  } else {
    document.body.style.overflow = saved_overflow;
  }


  // Update toolbar class
  var wrap = cm.getWrapperElement();

  if(!/fullscreen/.test(wrap.previousSibling.className)) {
    wrap.previousSibling.className += " fullscreen";
  } else {
    wrap.previousSibling.className = wrap.previousSibling.className.replace(/\s*fullscreen\b/, "");
  }


  // Update toolbar button
  var toolbarButton = editor.toolbarElements.fullscreen;

  if(!/active/.test(toolbarButton.className)) {
    toolbarButton.className += " active";
  } else {
    toolbarButton.className = toolbarButton.className.replace(/\s*active\s*/g, "");
  }


  // Hide side by side if needed
  //var sidebyside = cm.getWrapperElement().nextSibling;
  //if(/editor-preview-active-side/.test(sidebyside.className))
  //  toggleSideBySide(editor);
}

/**
 * Undo action.
 */
function undo(editor) {
  var cm = editor.codemirror;
  cm.undo();
  cm.focus();
}


/**
 * Redo action.
 */
function redo(editor) {
  var cm = editor.codemirror;
  cm.redo();
  cm.focus();
}

// Merge the properties of one object into another.
function _mergeProperties(target, source) {
  for(var property in source) {
    if(source.hasOwnProperty(property)) {
      if(source[property] instanceof Array) {
        target[property] = source[property].concat(target[property] instanceof Array ? target[property] : []);
      } else if(
        source[property] !== null &&
        typeof source[property] === "object" &&
        source[property].constructor === Object
      ) {
        target[property] = _mergeProperties(target[property] || {}, source[property]);
      } else {
        target[property] = source[property];
      }
    }
  }

  return target;
}

// Merge an arbitrary number of objects into one.
function extend(target) {
  for(var i = 1; i < arguments.length; i++) {
    target = _mergeProperties(target, arguments[i]);
  }

  return target;
}

/**
 * Interface
 */
function SimpleEditor(options) {
  // Handle options parameter
  options = options || {};


  // Used later to refer to it"s parent
  options.parent = this;

  // Find the textarea to use
  if(options.element) {
    this.element = options.element;
  } else if(options.element === null) {
    // This means that the element option was specified, but no element was found
    console.log("SimpleEditor: Error. No element was found.");
    return;
  }

  // Handle toolbar
  if(options.toolbar === undefined) {
    // Initialize
    options.toolbar = [];
  }

  // Handle status bar
  if(!options.hasOwnProperty("status")) {
    options.status = ["autosave", "lines", "words", "cursor"];
  }

  // Set default options for parsing config
  options.parsingConfig = extend({
    highlightFormatting: true // needed for toggleCodeBlock to detect types of code
  }, options.parsingConfig || {});

  // Merging the shortcuts, with the given options
  options.shortcuts = extend({}, shortcuts, options.shortcuts || {});

  // Change unique_id to uniqueId for backwards compatibility
  if(options.autosave != undefined && options.autosave.unique_id != undefined && options.autosave.unique_id != "")
    options.autosave.uniqueId = options.autosave.unique_id;

  // Update this options
  this.options = options;


  this.toolbarRegister = SimpleEditor.toolbarRegister;

  // Auto render
  this.render();


  // The codemirror component is only available after rendering
  // so, the setter for the initialValue can only run after
  // the element has been rendered
  if(options.initialValue && (!this.options.autosave || this.options.autosave.foundSavedValue !== true)) {
    this.value(options.initialValue);
  }
}

/**
 * Render editor to the given element.
 */
SimpleEditor.prototype.render = function(el) {
  if(!el) {
    el = this.element || document.getElementsByTagName("textarea")[0];
  }

  if(this._rendered && this._rendered === el) {
    // Already rendered.
    return;
  }

  this.element = el;
  var options = this.options;

  var self = this;
  var keyMaps = {};

  for(var key in options.shortcuts) {
    // null stands for "do not bind this command"
    if(options.shortcuts[key] !== null && bindings[key] !== null) {
      (function(key) {
        keyMaps[fixShortcut(options.shortcuts[key])] = function() {
          bindings[key](self);
        };
      })(key);
    }
  }

  keyMaps["Esc"] = function(cm) {
    if(cm.getOption("fullScreen")) toggleFullScreen(self);
  };

  document.addEventListener("keydown", function(e) {
    e = e || window.event;

    if(e.keyCode == 27) {
      if(self.codemirror.getOption("fullScreen")) toggleFullScreen(self);
    }
  }, false);

  var mode, backdrop;
  if(options.spellChecker !== false) {
    mode = "spell-checker";
    backdrop = options.parsingConfig;

    CodeMirrorSpellChecker({
      codeMirrorInstance: CodeMirror
    });
  } else {
    mode = options.parsingConfig;
  }

  this.codemirror = CodeMirror.fromTextArea(el, {
    mode: mode,
    backdrop: backdrop,
    theme: "screen",
    tabSize: (options.tabSize != undefined) ? options.tabSize : 2,
    indentUnit: (options.tabSize != undefined) ? options.tabSize : 2,
    indentWithTabs: (options.indentWithTabs === false) ? false : true,
    lineNumbers: !false,
    autofocus: (options.autofocus === true) ? true : false,
    extraKeys: keyMaps,
    lineWrapping: true,//(options.lineWrapping === false) ? false : true,
    allowDropFileTypes: ["text/plain"],
    placeholder: options.placeholder || el.getAttribute("placeholder") || "",
    styleSelectedText: (options.styleSelectedText != undefined) ? options.styleSelectedText : true
  });

  /*var charWidth = this.codemirror.defaultCharWidth(), basePadding = 4;
  this.codemirror.on("renderLine", function(cm, line, elt) {
    var off = CodeMirror.countColumn(line.text, null, cm.getOption("tabSize")) * charWidth;
    elt.style.textIndent = "-" + off + "px";
    elt.style.paddingLeft = (basePadding + off) + "px";
  });
  this.codemirror.refresh();*/

  if(options.forceSync === true) {
    var cm = this.codemirror;
    cm.on("change", function() {
      cm.save();
    });
  }

  this.gui = {};

  if(options.toolbar !== false) {
    this.gui.toolbar = this.createToolbar();
  }
  if(options.status !== false) {
    this.gui.statusbar = this.createStatusbar();
  }
  if(options.autosave != undefined && options.autosave.enabled === true) {
    this.autosave();
  }

  this._rendered = this.element;


  // Fixes CodeMirror bug (#344)
  var temp_cm = this.codemirror;
  setTimeout(function() {
    temp_cm.refresh();
  }.bind(temp_cm), 0);
};

// Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem throw QuotaExceededError. We're going to detect this and set a variable accordingly.
function isLocalStorageAvailable() {
  if(typeof localStorage === "object") {
    try {
      localStorage.setItem("simed_localStorage", 1);
      localStorage.removeItem("simed_localStorage");
    } catch(e) {
      return false;
    }
  } else {
    return false;
  }

  return true;
}

SimpleEditor.prototype.autosave = function() {
  if(isLocalStorageAvailable()) {
    var simplemde = this;

    if(this.options.autosave.uniqueId == undefined || this.options.autosave.uniqueId == "") {
      console.log("SimpleEditor: You must set a uniqueId to use the autosave feature");
      return;
    }

    if(simplemde.element.form != null && simplemde.element.form != undefined) {
      simplemde.element.form.addEventListener("submit", function() {
        localStorage.removeItem("simed_" + simplemde.options.autosave.uniqueId);
      });
    }

    if(this.options.autosave.loaded !== true) {
      if(typeof localStorage.getItem("simed_" + this.options.autosave.uniqueId) == "string" && localStorage.getItem("simed_" + this.options.autosave.uniqueId) != "") {
        this.codemirror.setValue(localStorage.getItem("simed_" + this.options.autosave.uniqueId));
        this.options.autosave.foundSavedValue = true;
      }

      this.options.autosave.loaded = true;
    }

    localStorage.setItem("simed_" + this.options.autosave.uniqueId, simplemde.value());

    var el = document.getElementById("autosaved");
    if(el != null && el != undefined && el != "") {
      var d = new Date();
      var hh = d.getHours();
      var m = d.getMinutes();
      var dd = "am";
      var h = hh;
      if(h >= 12) {
        h = hh - 12;
        dd = "pm";
      }
      if(h == 0) {
        h = 12;
      }
      m = m < 10 ? "0" + m : m;

      el.innerHTML = "Autosaved: " + h + ":" + m + " " + dd;
    }

    this.autosaveTimeoutId = setTimeout(function() {
      simplemde.autosave();
    }, this.options.autosave.delay || 10000);
  } else {
    console.log("SimpleEditor: localStorage not available, cannot autosave");
  }
};

SimpleEditor.prototype.clearAutosavedValue = function() {
  if(isLocalStorageAvailable()) {
    if(this.options.autosave == undefined || this.options.autosave.uniqueId == undefined || this.options.autosave.uniqueId == "") {
      console.log("SimpleEditor: You must set a uniqueId to clear the autosave value");
      return;
    }

    localStorage.removeItem("simed_" + this.options.autosave.uniqueId);
  } else {
    console.log("SimpleEditor: localStorage not available, cannot autosave");
  }
};

SimpleEditor.prototype.createToolbar = function(items) {
  items = items || this.options.toolbar;

  if(!items || items.length === 0) {
    return;
  }

  items = this.toolbarRegister.collect(items)

  var bar = document.createElement("div");
  bar.className = "editor-toolbar";

  var self = this;

  var toolbarData = {};
  self.toolbar = items;

  var i;
  for(i = 0; i < items.length; i++) {
    if(items[i].name == "guide" && self.options.toolbarGuideIcon === false)
      continue;

    if(self.options.hideIcons && self.options.hideIcons.indexOf(items[i].name) != -1)
      continue;

    // Fullscreen does not work well on mobile devices (even tablets)
    // In the future, hopefully this can be resolved
    if((items[i].name == "fullscreen" || items[i].name == "side-by-side") && isMobile())
      continue;


    // Don't include trailing separators
    if(items[i] === "|") {
      var nonSeparatorIconsFollow = false;

      for(var x = (i + 1); x < items.length; x++) {
        if(items[x] !== "|" && (!self.options.hideIcons || self.options.hideIcons.indexOf(items[x].name) == -1)) {
          nonSeparatorIconsFollow = true;
        }
      }

      if(!nonSeparatorIconsFollow)
        continue;
    }


    // Create the icon and append to the toolbar
    (function(item) {
      var el;
      if(item === "|") {
        el = createSep();
      } else {
        el = createIcon(item, self.options.toolbarTips, self.options.shortcuts);
      }

      // bind events, special for info
      if(item.action) {
        if(typeof item.action === "function") {
          el.onclick = function(e) {
            e.preventDefault();
            item.action(self);
          };
        } else if(typeof item.action === "string") {
          el.href = item.action;
          el.target = "_blank";
        }
      }

      toolbarData[item.name || item] = el;
      bar.appendChild(el);
    })(items[i]);
  }

  self.toolbarElements = toolbarData;

  var cm = this.codemirror;
  cm.on("cursorActivity", function() {
    /*var stat = getState(cm);

    for(var key in toolbarData) {
      (function(key) {
        var el = toolbarData[key];
        if(stat[key]) {
          el.className += " active";
        } else if(key != "fullscreen" && key != "side-by-side") {
          el.className = el.className.replace(/\s*active\s*//*g, "");
        }
      })(key);
    }*/
  });

  var cmWrapper = cm.getWrapperElement();
  cmWrapper.parentNode.insertBefore(bar, cmWrapper);
  return bar;
};

SimpleEditor.prototype.createStatusbar = function(status) {
  // Initialize
  status = status || this.options.status;
  var options = this.options;
  var cm = this.codemirror;


  // Make sure the status variable is valid
  if(!status || status.length === 0)
    return;


  // Set up the built-in items
  var items = [];
  var i, onUpdate, defaultValue;

  for(i = 0; i < status.length; i++) {
    // Reset some values
    onUpdate = undefined;
    defaultValue = undefined;


    // Handle if custom or not
    if(typeof status[i] === "object") {
      items.push({
        className: status[i].className,
        defaultValue: status[i].defaultValue,
        onUpdate: status[i].onUpdate
      });
    } else {
      var name = status[i];

      /*if(name === "words") {
        defaultValue = function(el) {
          el.innerHTML = wordCount(cm.getValue());
        };
        onUpdate = function(el) {
          el.innerHTML = wordCount(cm.getValue());
        };
      } else*/ if(name === "lines") {
        defaultValue = function(el) {
          el.innerHTML = cm.lineCount();
        };
        onUpdate = function(el) {
          el.innerHTML = cm.lineCount();
        };
      } else if(name === "cursor") {
        defaultValue = function(el) {
          el.innerHTML = "0:0";
        };
        onUpdate = function(el) {
          var pos = cm.getCursor();
          el.innerHTML = pos.line + ":" + pos.ch;
        };
      } else if(name === "autosave") {
        defaultValue = function(el) {
          if(options.autosave != undefined && options.autosave.enabled === true) {
            el.setAttribute("id", "autosaved");
          }
        };
      }

      items.push({
        className: name,
        defaultValue: defaultValue,
        onUpdate: onUpdate
      });
    }
  }


  // Create element for the status bar
  var bar = document.createElement("div");
  bar.className = "editor-statusbar";


  // Create a new span for each item
  for(i = 0; i < items.length; i++) {
    // Store in temporary variable
    var item = items[i];


    // Create span element
    var el = document.createElement("span");
    el.className = item.className;


    // Ensure the defaultValue is a function
    if(typeof item.defaultValue === "function") {
      item.defaultValue(el);
    }


    // Ensure the onUpdate is a function
    if(typeof item.onUpdate === "function") {
      // Create a closure around the span of the current action, then execute the onUpdate handler
      this.codemirror.on("update", (function(el, item) {
        return function() {
          item.onUpdate(el);
        };
      }(el, item)));
    }


    // Append the item to the status bar
    bar.appendChild(el);
  }


  // Insert the status bar into the DOM
  var cmWrapper = this.codemirror.getWrapperElement();
  cmWrapper.parentNode.insertBefore(bar, cmWrapper.nextSibling);
  return bar;
};

/**
 * Bind static methods for exports.
 */
SimpleMDE.toggleFullScreen = toggleFullScreen;

/**
 * Get or set the text content.
 */
SimpleEditor.prototype.value = function(val) {
  if(val === undefined) {
    return this.codemirror.getValue();
  } else {
    this.codemirror.getDoc().setValue(val);
    return this;
  }
};

/**
 * Bind instance methods for exports.
 */
SimpleEditor.prototype.undo = function() {
  undo(this);
};
SimpleEditor.prototype.redo = function() {
  redo(this);
};
SimpleEditor.prototype.toggleFullScreen = function() {
  toggleFullScreen(this);
};
SimpleEditor.prototype.isFullscreenActive = function() {
  var cm = this.codemirror;

  return cm.getOption("fullScreen");
};

SimpleEditor.prototype.getState = function() {
  var cm = this.codemirror;

  return getState(cm);
};

SimpleEditor.prototype.toTextArea = function() {
  var cm = this.codemirror;
  var wrapper = cm.getWrapperElement();

  if(wrapper.parentNode) {
    if(this.gui.toolbar) {
      wrapper.parentNode.removeChild(this.gui.toolbar);
    }
    if(this.gui.statusbar) {
      wrapper.parentNode.removeChild(this.gui.statusbar);
    }
  }

  cm.toTextArea();

  if(this.autosaveTimeoutId) {
    clearTimeout(this.autosaveTimeoutId);
    this.autosaveTimeoutId = undefined;
    this.clearAutosavedValue();
  }
};

function ToolbarRegister(){
  this.registeredItems = {};
}

ToolbarRegister.prototype.collect = function(items) {
  var i;
  for(i = 0; i < items.length; i++) {
    if(this.registeredItems[items[i]] != undefined) {
      items[i] = this.registeredItems[items[i]];
    }
  }
  return items;
};

ToolbarRegister.prototype.register = function() {
  var i;
  for(i = 0; i < arguments.length; i++) {
    this.registeredItems[arguments[i].name] = arguments[i];
  }
};

var toolbarRegister = new ToolbarRegister();

toolbarRegister.register({
    name: "fullscreen",
    action: toggleFullScreen,
    className: "fa fa-arrows-alt no-disable no-mobile",
    title: "Toggle Fullscreen",
    default: true
  });

SimpleEditor.toolbarRegister = toolbarRegister;

window.SimpleEditor = SimpleEditor;
//})();
