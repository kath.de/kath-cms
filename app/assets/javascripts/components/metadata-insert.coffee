$ ()-> MetadataInsert.createToolbarIcon()

class MetadataRequest
  constructor: (insert, url) ->
    @insert = insert
    @url = url
    @success = false
    @completed = false
    @response = null
    @jqXhr = $.ajax({
          url: MetadataInsert.API_ENDPOINT,
          type: 'post',
          data: { url: @url }
          dataType: 'json',
          success: @onSuccess,
          error: @onError
        })

  onSuccess: (data) =>
    @success = true
    @completed = true
    @response = data
    @insert.onMetadataSuccess(this)

  onError: (xhr, status, error_message) =>
    @success = false
    @completed = true
    @error = error_message
    console.log arguments
    @insert.onMetadataError(this, error_message)

class MetadataInsert
  constructor: (editor)->
    @createModal()
    @editor = editor
    @doc = editor.codemirror.getDoc()
    @collectedData = {}
    @urlList = []

  createModal: ()->
    @modal = new tingle.modal({
        footer: true,
        stickyFooter: false,
        cssClass: ['add-entry-dialog'],
      })

    @textarea = document.createElement('textarea')
    @textarea.addEventListener 'paste', @onPaste
    @textarea.setAttribute 'placeholder', 'http://'
    @textarea.setAttribute 'rows', 15
    @modal.setContent(@textarea)
    @modal.addFooterBtn 'Schließen', 'tingle-btn btn-back tingle-btn--pull-left', @onModalClose
    @submitButton = @modal.addFooterBtn 'Metadaten importieren', 'tingle-btn tingle-btn--primary tingle-btn--pull-right', @onModalSubmit

  open: ()->
    @modal.open()
    @textarea.focus()

  onModalClose: () =>
    @modal.close()

  onModalSubmit: () =>
    @disableInterface()
    @collectMetadata @textarea.value

  disableInterface: (disable=true) ->
    @textarea.disabled = disable
    @submitButton.disabled = disable

  onPaste: () =>
    # paste event is triggered before the text is inserted, setTimeout delays our modification after
    # the insert
    setTimeout @onAfterPaste, 0

  onAfterPaste: () =>
    @textarea.value = @textarea.value + "\n"

  onModalInput: () =>
    console.log @textarea.value
    input = @textarea.value
    unless input.charAt(input.length - 1)
      console.log "adding newline"
      @textarea.value += "\n"

  collectMetadata: (input) ->
    console.log arguments

    urls = input.split(/\n/)
    # TODO: Add validation

    for url in urls
      @retrieveMetadata(url) unless url.match /^\s*$/

  insertEntries: (content) ->
    @editor.codemirror.execCommand 'goLineEnd'
    @doc.replaceSelection("\n" + content.join("\n"))
    @editor.codemirror.focus()

  retrieveMetadata: (url) ->
    @collectedData[url] = new MetadataRequest(this, url)
    console.log "retrieving #{url}"
    console.log @collectedData

  onMetadataError: (request, error_message) =>
    console.log "error", arguments

    @disableInterface(false)
    alert("Beim automatisierten Auslesen ist ein Fehler aufgetreten: #{error_message}. Bitte versuchen Sie es nochmal ggf. mit korrigiertem URL.")

  onMetadataSuccess: (request) =>
    console.log "success", request
    @notifyMetadataResponse(request.response)
    @checkIfAllCompleted()

  notifyMetadataResponse: (response) =>


  checkIfAllCompleted: () ->
    for url, request of @collectedData
      return unless request.completed
      console.log "#{url} has completed"

    # if we have not returned, then all urls are completed
    content = for url, request of @collectedData
      request.response.yaml

    console.log "content", content
    @insertEntries content

    @modal.close()

MetadataInsert.API_ENDPOINT = '/metadata.json'

MetadataInsert.createToolbarIcon = () ->
  SimpleEditor.toolbarRegister.register {
    name: 'add-entry',
    action: (editor) ->
      new MetadataInsert(editor).open()

    className: "fa fa-plus-square",
    title: "Eintrag hinzufügen"
  }

embedly = (url, cb) ->
  embedlyKey = "5190ba1ae80646b9aa6ac6b61db4a769"

  $.ajax({
    url: "//api.embed.ly/1/extract?key=#{embedlyKey}&url=#{encodeURIComponent(url)}",
    dataType: 'jsonp',
    success: (data) ->
      console.log data
      cb(data)
    })

generateYAML = (data) ->
  fields = {
    url: (data) -> data.url
    title: (data) -> "#{data.title.split('|')[0]}"
    subtitle: (data) -> data.description
    source: (data) -> provider(data.provider_url, data.provider_name)
    date: (data) -> new Date(data.published).toISOString().split('T')[0]
    tags: (data) ->
      return unless data.keywords
      tags = ("'#{tag.name}'" for tag in data.keywords)
      "[#{tags.join(', ')}]"
  }
  #console.log fields

  fields = for key, value of fields
    "#{key}: #{value(data)?.trim?() || ''}"

  "\n- " + fields.join("\n\t")

PROVIDERS = {
  'http://www.dw.com': 'Deutsche Welle'
}
provider = (url, default_name) ->
  PROVIDERS[url] || default_name


window.MetadataInsert = MetadataInsert
