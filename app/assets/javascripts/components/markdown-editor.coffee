
# Er sagte: Hallo Peter, der antwortet:  Aber dann nicht mehr.

$ ()->
  for element in document.getElementsByClassName('markdown-editor')
    sonderzeichen = {
      'anfuehrungszeichen': {
        name: 'anfuehrungszeichen',
        action: (editor) ->
          console.log editor
          doc = editor.codemirror.doc
          currentylSelected = doc.getSelection()
          doc.replaceSelection("„#{currentylSelected}“", "around")

          console.log currentylSelected
          #if currentylSelected.length
            # now select inserted text
          cursorFrom = doc.getCursor("from")
          cursorTo = doc.getCursor("to")
          doc.setSelection({ line: cursorFrom.line, ch: cursorFrom.ch + 1},
                           { line: cursorTo.line,   ch: cursorTo.ch - 1})
          #else
            # or place cursor in between empty quotation marks
          #  cursorFrom = doc.getCursor("from")
          console.log "cursor: ", cursorFrom, cursorTo
          #  doc.setCursor({ line: cursorFrom.line, ch: cursorFrom.ch + 1})

          editor.codemirror.focus()

        className: "fa fa-local  fa-local-anfuehrungszeichen",
        title: "Anführungszeichen einfügen"
      },
      'ellipsis': {
        name: 'ellipsis',
        action: (editor) ->
          doc = editor.codemirror.doc
          doc.replaceSelection("…")

          editor.codemirror.focus()

        className: "fa fa-local fa-local-ellipsis",
        title: "Ellipse einfügen"
      },
      'ndash': {
        name: 'ndash',
        action: (editor) ->
          doc = editor.codemirror.doc
          doc.replaceSelection("–")

          editor.codemirror.focus()

        className: "fa fa-local fa-local-ndash",
        title: "Gedankenstrich einfügen"
      }
    }

    simple_toolbar = ['bold', 'italic', '|',
                      sonderzeichen['anfuehrungszeichen'], sonderzeichen['ellipsis'],
                      sonderzeichen['ndash'], '|',
                      'link', '|',
                      'preview',
                     ]

    normal_toolbar = ['bold', 'italic', 'heading', '|',
                      sonderzeichen['anfuehrungszeichen'], sonderzeichen['ellipsis'],
                      sonderzeichen['ndash'], '|',
                      'quote', 'unordered-list', 'ordered-list', '|',
                      'link', 'image', '|',
                      'preview', 'side-by-side', 'fullscreen', 'guide',
                    ]

    do (element) ->
      toolbar = if element.getAttribute('data-toolbar') == 'simple'
            simple_toolbar
          else
            normal_toolbar

      editor = new SimpleMDE({
          element: element,
          autosave: {
            enabled: false,
            uniqueId: element.getAttribute('data-uid')
          },
          spellChecker: false, # TODO: currently only en_US supported,
          toolbar: toolbar,
          shortcuts: {
          # TODO: Shortcuts for custom actions are currently not supported
          # https://github.com/NextStepWebs/simplemde-markdown-editor/issues/248
            #'ndash': "AltGr--",
            #'anfuehrungszeichen': "Alt-2",
            #"ellipsis", "Str"
          }
        })
      window.editorToolbars.register editor.gui.toolbar
#„“–„“…
