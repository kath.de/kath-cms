$ ()->
  registerToolbarAdditions(SimpleEditor);

  for element in document.querySelectorAll('[data-editor=yaml-editor]')
    do (element) ->

      #editor = CodeMirror.fromTextArea(element, {
      #  mode: { name: 'yaml' },
      #  theme: "paper",
      #  tabSize: 2,
      #  indentUnit: 2,
      #  lineNumbers: false,
#
      #  })*/
      toolbar = ['add-entry', '|', 'anfuehrungszeichen', 'ellipsis', 'ndash', '|', 'fullscreen'];

      editor = new SimpleEditor({
          element: element,
          autosave: {
            enabled: true,
            uniqueId: element.getAttribute('data-uid')
          },
          spellChecker: false, # TODO: currently only en_US supported,
          toolbar: toolbar,
          shortcuts: {},
          parsingConfig: {
            name: "yaml"
          },
          indentWithTabs: false
        })
      editor.codemirror.setOption 'styleActiveLine', true
      editor.codemirror.setOption 'tabSize', 2
      editor.codemirror.setOption 'extraKeys', {
            "Tab": (cm) ->
              cm.replaceSelection "  " , "end"
           }

      window.editorToolbars.register editor.gui.toolbar
      console.log editor

registerToolbarAdditions = (editor) ->
  editor.toolbarRegister.register {
      name: 'anfuehrungszeichen',
      action: (editor) ->
        doc = editor.codemirror.doc
        currentylSelected = doc.getSelection()
        doc.replaceSelection("„#{currentylSelected}“", "around")

        cursorFrom = doc.getCursor("from")
        cursorTo = doc.getCursor("to")
        doc.setSelection({ line: cursorFrom.line, ch: cursorFrom.ch + 1},
                         { line: cursorTo.line,   ch: cursorTo.ch - 1})

        console.log "cursor: ", cursorFrom, cursorTo

        editor.codemirror.focus()

      className: "fa fa-local  fa-local-anfuehrungszeichen",
      title: "Anführungszeichen einfügen"
    },{
      name: 'ellipsis',
      action: (editor) ->
        doc = editor.codemirror.doc
        doc.replaceSelection("…")

        editor.codemirror.focus()

      className: "fa fa-local fa-local-ellipsis",
      title: "Ellipse einfügen"
    }, {
      name: 'ndash',
      action: (editor) ->
        doc = editor.codemirror.doc
        doc.replaceSelection("–")

        editor.codemirror.focus()

      className: "fa fa-local fa-local-ndash",
      title: "Gedankenstrich einfügen"
    }

