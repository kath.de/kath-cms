
class EditorToolbarRegister
  constructor: ()->
    @items = []

  register: (toolbarElem) ->
    @items.push {
      toolbar: $(toolbarElem),
      editor: toolbarElem.nextSibling
    }

  handleScrollEvent: () =>
    for elem in @items
      console.log elem
      rect = elem.editor.getBoundingClientRect();
      toolbarHeight = elem.toolbar.height()
      console.log rect
      if rect.top < toolbarHeight && rect.bottom > window.innerHeight / 2
        elem.toolbar.addClass 'editor-toolbar--fixed'
        elem.toolbar.css left: rect.left, width: rect.width
        elem.editor.setAttribute 'css', "margin-top: #{toolbarHeight}px"

      else
        elem.toolbar.removeClass 'editor-toolbar--fixed'
        elem.toolbar.css left: '', width: ''
        elem.editor.setAttribute 'css', ""

window.editorToolbars = new EditorToolbarRegister()

# Returns a function, that, as long as it continues to be invoked, will not
# be triggered. The function will be called after it stops being called for
# N milliseconds. If `immediate` is passed, trigger the function on the
# leading edge, instead of the trailing.
debounce = (func, wait, immediate) ->
  () ->
    context = @
    args = arguments

    callNow = immediate && !@timeout
    clearTimeout(@timeout)

    later = () ->
      context.timeout = null
      func.apply(context, args) unless immediate

    @timeout = setTimeout(later, wait)

    func.apply(context, args) if callNow

document.addEventListener 'scroll', debounce(window.editorToolbars.handleScrollEvent, 100)
