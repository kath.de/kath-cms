# Need to require this before anything else to be usable in kathde-models/Tagesreport
require 'activerecord_json_validator'

# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
