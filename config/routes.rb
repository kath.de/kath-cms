Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :admin do
    Administrate::NamespaceWithFilter::INCLUDED_RESSOURCES.each do |model|
      resources model do
        member do
          post :publish, to: 'publish#create'
          delete :publish, to: 'publish#destroy', as: :unpublish

          resource :versions, only: [:index], as: "#{model}_versions"
        end
      end
    end

    resources :versions, only: [:index, :show]
  end

  post :metadata, to: 'metadata#create'

  get :docs, to: 'docs#index', as: 'docs'
  get 'docs/*path', to: 'docs#show', as: 'doc'

  root to: 'dashboard#show'
end
