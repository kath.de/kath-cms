require File.expand_path('../production.rb', __FILE__)

Rails.application.configure do
  config.preview_base_url = 'https://kath-de-staging.rabanus.kath.de/'
end
