#PaperTrail.config.track_associations = true

require 'entry_with_paper_trail'

# Dunno why this is necessary, but without it, PaperTrail fails to find the versions association on
# Author when creating a Kommentar instance.
Author.prepend EntryWithPaperTrail
Image.prepend EntryWithPaperTrail
Kommentar.prepend EntryWithPaperTrail

#Entry.prepend EntryWithPaperTrail
