# frozen_string_literal: true

ActiveSupport.on_load(:active_record) do
  require 'administrate/field/base'
  require 'administrate/namespace'

  Administrate::Page::Base.prepend(Administrate::PageWithResourceForField)
  Administrate::Field::Base.prepend(Administrate::FieldWithResource)
  Administrate::Namespace.prepend(Administrate::NamespaceWithFilter)

  Administrate::Field::ActiveStorage.prepend(Administrate::ActiveStorageFieldBackend)
end
