# frozen_string_literal: true

require 'entry/persist'
require 'entry/new'

require 'tagesreport/persist'
require 'tagesreport/new'

require 'publisher/persist'

require 'settings/persist'
