# frozen_string_literal: true

ActionView::Template::Handlers::ERB.escape_ignore_list << 'text/yaml'
