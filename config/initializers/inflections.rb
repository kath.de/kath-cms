# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end

ActiveSupport::Inflector.inflections do |inflect|
  inflect.singular 'kommentare', 'kommentar'
  inflect.plural 'kommentar', 'kommentare'
  inflect.irregular 'tagesreport', 'tagesreporte'
  inflect.irregular 'wochenrueckblick', 'wochenrueckblicke'
  inflect.uncountable %w[Pressespiegel]
  inflect.uncountable %w[settings]
end

ActiveSupport::Inflector.inflections(:de) do |inflect|
  inflect.singular 'kommentare', 'kommentar'
  inflect.plural 'kommentar', 'kommentare'
  inflect.singular 'Einträge', 'Eintrag'
  inflect.plural 'Eintrag', 'Einträge'
  inflect.irregular 'Tagesreport', 'Tagesreporte'
  inflect.irregular 'wochenrueckblick', 'wochenrueckblicke'
  inflect.uncountable %w[Pressespiegel]
end
