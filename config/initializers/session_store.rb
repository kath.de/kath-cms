# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

unless Rails.env == 'test'
  require 'rack-cas/session_store/rails/redis'
  Rails.application.config.session_store ActionDispatch::Session::RackCasRedisStore
end
