
# frozen_string_literal: true

Rails.application.config.time_zone = 'Berlin'
I18n.available_locales = %i[de en]
I18n.default_locale = :de
I18n.locale = :de

Rails.application.config.i18n.default_locale = I18n.default_locale
Rails.application.config.i18n.locale = I18n.locale
