# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Publishing', type: :request do
  include Fakecas::RequestHelpers

  before do
    use_authorized_account
  end

  describe 'unpublished Kommentar' do
    subject(:unpublished_kommentar) { create(:kommentar, :unpublished) }

    it 'is published' do
      expect { publish }.to change { unpublished_kommentar.reload.published? }.from(false).to(true)
    end

    it 'is unpublished and does not change' do
      expect { unpublish }.not_to change { unpublished_kommentar.published? }
    end

    it 'is unpublished and shows error message' do
      unpublish
      expect(flash[:notice]).to match(/ist bereits unveröffentlicht/)
    end
  end

  describe 'published Kommentar' do
    subject(:published_kommentar) { create(:kommentar, :published) }

    it 'is published in the first place' do
      is_expected.to be_published
    end

    describe 'about to be published' do
      it 'does not change' do
        expect { publish }.not_to change { published_kommentar.published_at }
      end

      it 'shows error message' do
        publish
        expect(flash[:notice]).to match(/ist bereits veröffentlicht/)
      end
    end

    describe 'about to be unpublished' do
      it 'is unpublished' do
        expect { unpublish }.to change {
          published_kommentar.reload.published?
        }.from(true).to(false)
      end
    end
  end

  def publish
    post publish_admin_kommentar_path(id: subject.id)
  end

  def unpublish
    delete unpublish_admin_kommentar_path(id: subject.id)
  end
end
