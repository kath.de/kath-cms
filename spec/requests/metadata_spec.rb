# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Metadata', type: :request do
  include Fakecas::RequestHelpers

  before do
    use_authorized_account
  end

  describe 'receives a url' do
    let(:url) { 'http://example.com/test_page' }

    before do
      post '/metadata', params: { url: url }
    end

    it('is success') { expect(response).to be_successful }

    it('correctly identifies queried url') do
      expect(json_response['url']).to eq('http://example.com/test_page')
    end

    # context 'invalid url' do
    #  let(:url) { 'foobar' }

    # This test does not work because of fakeweb
    # it('fails with error message') {
    # allow(Metainspector).to receive(:new).with(url).
    #      an_raise(SocketError, 'getaddrinfo: Name or service not known')
    #  pending

    # expect(json_response['status']).to eq('error')
    # expect(json_response['error']).to eq(%{getaddrinfo: Name or service not known})
    # }
    # end

    context '404 url' do
      # It is debateable if a 404 should be an error. But it does not actually break anything,
      # just provides usually very litte metadata.
      let(:url) { 'http://example.com/404' }

      it('does not fail') { expect(json_response['status']).to eq('success') }
      # it('has error message') { pending; expect(json_response['error']).to eq(
      #     %{Absolute URI missing hierarchical segment: 'http://'}) }
    end

    context 'empty url' do
      let(:url) { '' }

      it('fails') { expect(json_response['status']).to eq('error') }
      it('has error message') do
        expect(json_response['error']).to eq(
          %(Absolute URI missing hierarchical segment: 'http://'),
        )
      end
    end
  end

  describe 'creation of publisher record' do
    let(:url) do
      'http://www.rp-online.de/kultur/'\
                'kardinal-nennt-trennung-der-kirchen-schmerzlich-aid-1.6345559'
    end
    let(:run_request) { post '/metadata', params: { url: url } }

    before { Publisher.delete_all }

    def publisher
      Publisher.by_url('rp-online.de')
    end

    context 'publisher not yet existant' do
      it 'creates new publisher' do
        expect { run_request }.to change { publisher.present? }.from(false).to(true)
      end

      it 'sets title' do
        run_request
        expect(publisher.title).to eq('RP ONLINE')
      end
    end

    context 'publisher exists' do
      let!(:existing_publisher) do
        create(:publisher, domain: 'rp-online.de', title: 'Existing Title')
      end

      it 'does not create new publisher' do
        expect { run_request }.not_to change(Publisher, :count)
      end

      it 'does not change title' do
        expect { run_request }.not_to change(publisher, :title).from(existing_publisher.title)
      end
    end
  end
  describe 'yaml generation based on actual samples' do
    let(:yaml) do
      post '/metadata', params: { url: url }
      json_response['yaml']
    end
    let(:today) { Time.zone.now.strftime('%Y-%m-%d') }

    context 'for Radio Vatican website' do
      let(:url) do
        'http://de.radiovaticana.va/news/2016/10/20/'\
                  'heiliges_land_%C3%B6kumenische_wallfahrt_macht_hoffnung/1266502'
      end

      it('succeeds') do
        yaml
        expect(json_response['status']).to eq('success')
      end

      # NOTE: crappy html in description delivered by provider
      it do
        expect(yaml).to eq(
          <<-YAML.strip_heredoc
            - url: "#{url}"
              titel: "Heiliges Land: Ökumenische Wallfahrt macht Hoffnung"
              untertitel: "Heiliges Land: Ökumenische Wallfahrt macht Hoffnung - Oft sagt man &amp;uuml;ber &amp;Ouml;kumene: An der Basis ist alles gekl&amp;auml;rt, eigentlich m&amp;uuml;ssen sich nur noch die Kirchen einigen."
              datum: 2016-10-20
          YAML
        )
      end
    end

    context 'for Spiegel-Online' do
      let(:url) do
        'http://www.spiegel.de/kultur/gesellschaft/filmdienst-deutschlands-aelteste-filmzeitschrift-vor-dem-aus-a-1117972.html'
      end

      it do
        expect(yaml).to eq(
          <<-YAML.strip_heredoc
            - url: "#{url}"
              titel: "Katholischer \"Filmdienst\": Deutschlands älteste Filmzeitschrift vor dem Aus"
              untertitel: "Das von der katholischen Kirche finanzierte Magazin \"Filmdienst\" ist die älteste Filmzeitschrift Deutschlands."
              datum: 2016-10-24
          YAML
        )
      end
    end

    context 'for Domradio' do
      let(:url) do
        'https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/'\
                  'kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd'
      end

      it do
        expect(yaml).to eq(
          <<-YAML.strip_heredoc
            - url: "#{url}"
              titel: "Kirchentagspräsidentin zum Reformator und Umgang mit der AfD"
              untertitel: "Das klingt doch ein wenig überraschend: Reformator Martin Luther hätte nach den Worten von Kirchentagspräsidentin Christina Aus der Au heute Schwierigkeiten, zum Deutschen Evangelischen Kirchentag eingeladen zu werden."
          YAML
        )
      end
    end

    context 'for RP-Online' do
      let(:url) do
        'http://www.rp-online.de/kultur/'\
                  'kardinal-nennt-trennung-der-kirchen-schmerzlich-aid-1.6345559'
      end

      it do
        expect(yaml).to eq(
          <<-YAML.strip_heredoc
            - url: "#{url}"
              titel: "Jerusalem: Kardinal nennt Trennung der Kirchen 'schmerzlich'"
              untertitel: "Gemeinsam an den Heiligen Stätten beten, gemeinsam auf den Spuren von Jesus Christus wandern und gemeinsam in der Holocaustgedenkstätte Yad Vashem schweigen - auf der Pilgerreise von Vertretern der Deutschen Bischofskonferenz (DBK) und der Evangelischen Kirche in Deutschland (EKD) stand das Gemeinsame, nicht das Trennende im Mittelpunkt."
          YAML
        )
      end
    end

    context 'for idea' do
      let(:url) do
        'http://www.idea.de/frei-kirchen/detail/'\
                  'juden-und-muslime-sprechen-zum-reformationstag-98662.html'
      end

      it do
        expect(yaml).to eq(
          <<-YAML.strip_heredoc
            - url: "#{url}"
              titel: "Juden und Muslime sprechen zum Reformationstag"
              untertitel: "Das Theologische Studienseminar der Vereinigten Evangelisch-Lutherischen Kirche Deutschlands lädt zu einer „interreligiösen Schriftauslegung“ ein, bei der neben Pfarrern auch Juden und Muslime Texte aus dem Neuen Testament sowie aus dem Koran und der hebräischen Bibel auslegen – und erntet Kritik."
          YAML
        )
      end
    end
  end
  # rubocop:enable RSpec/ExampleLength
end
