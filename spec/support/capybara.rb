# frozen_string_literal: true

Capybara.javascript_driver = :webkit
Capybara.ignore_hidden_elements = false
