# frozen_string_literal: true

require 'rack_session_access/capybara'

module Fakecas
  module Base
    def authorized_session_data(account = nil)
      account ||= create(:account)
      { 'cas' => { 'user' => account.username, 'extra_attributes' => {} } }
    end

    def use_authorized_account(account = nil)
      set_rack_session(authorized_session_data(account))
    end
  end

  class Exception < RuntimeError; end

  module FeatureHelpers
    include Base

    delegate :set_rack_session, to: :page
  end

  module RequestHelpers
    include Base

    # Modified rack_session_access/lib/rack_session_access/capybara.rb to play with request spec
    def set_rack_session(hash) # rubocop:disable Style/AccessorMethodName
      sessiondata = ::RackSessionAccess.encode(hash)

      get ::RackSessionAccess.edit_path
      throw Fakecas::Exception(response.body) unless response.body =~ /Update rack session/

      doc = Nokogiri::HTML(response.body)
      form = doc.xpath('//form').first
      data = {}
      form.xpath('input').each do |input|
        data[input['name']] = input['value']
      end
      data['data'] = sessiondata
      post form['action'], params: data

      get response.header['Location']
      throw Fakecas::Exception(response.body) unless response.body =~ /Rack session data/
    end

    # def get_rack_session
    #  visit ::RackSessionAccess.path + '.raw'
    #  has_content?("Raw rack session data")
    #  raw_data = find(:xpath, '//body/pre').text
    #  ::RackSessionAccess.decode(raw_data)
    # end

    # def get_rack_session_key(key)
    #  get_rack_session.fetch(key)
    # end
  end
end
