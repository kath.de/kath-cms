# frozen_string_literal: true

require 'fakeweb'

FakeWeb.allow_net_connect = false

def register_uri(uri, filename)
  file_path = "#{File.expand_path(File.dirname(__FILE__))}/../fixtures/response/#{filename}.response"

  FakeWeb.register_uri(:get, uri, response: File.read(file_path))
end

register_uri('http://example.com/test_page', 'test_page')
register_uri('http://example.com/another_test_page', 'another_test_page')
register_uri('http://de.radiovaticana.va/news/2016/10/20/heiliges_land_%C3%B6kumenische_wallfahrt_macht_hoffnung/1266502',
             'de.radiovaticana.va_news_heiliges_land_ökumenische_wallfahrt')
register_uri('https://www.theguardian.com/us-news/2016/oct/24/elizabeth-warren-hillary-clinton-nasty-women-donald-trump',
             'theguardian.com_elizabeth-warren-hillary-clinton')
register_uri('https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/eine-million-sterne-in-frankfurt.html',
             'bistumlimburg.de_eine-million-sterne-in-frankfurt')
register_uri('http://erzbistum-bamberg.de/nachrichten/zukunftsmusik-beim-neuen-geistlichen-lied/418ff0f2-d20b-463b-943d-60866f976829?mode=detail',
             'erzbistum-bamberg.de_zukunftsmusik-beim-neuen-geistlichen-lied')
register_uri('http://www.spiegel.de/kultur/gesellschaft/filmdienst-deutschlands-aelteste-filmzeitschrift-vor-dem-aus-a-1117972.html',
             'spiegel.de_filmdienst-deutschlands-aelteste-filmzeitschrift-vor-dem-aus')
register_uri('https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd',
             'domradio.de_kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd')
register_uri('http://www.rp-online.de/kultur/kardinal-nennt-trennung-der-kirchen-schmerzlich-aid-1.6345559',
             'rp-online.de_kardinal-nennt-trennung-der-kirchen-schmerzlich')

register_uri('http://www.idea.de/frei-kirchen/detail/juden-und-muslime-sprechen-zum-reformationstag-98662.html',
             'idea.de_juden-und-muslime-sprechen-zum-reformationstag')
register_uri('http://www.pro-medienmagazin.de/gesellschaft/kirche/detailansicht/aktuell/neue-spielraeume-des-evangeliums-schaffen-98027/',
             'pro-medienmagezin.de_neue-spielraeume-des-evangeliums-schaffen')
register_uri('http://example.com/404', '404')
