# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'User views tagesreport' do
  before do
    use_authorized_account
  end

  let(:tagesreport) { create(:tagesreport) }

  it 'shows preview link' do
    expect(tagesreport.payload['preview_token']).not_to be_present

    visit(admin_tagesreport_path(tagesreport))

    tagesreport.reload
    expect(tagesreport.payload['preview_token']).to be_present

    expect(page).to have_content('Vorschau')

    link = find_link('Vorschau')
    expect(link['href']).to include("preview_token=#{tagesreport.preview_token}")
  end
end
