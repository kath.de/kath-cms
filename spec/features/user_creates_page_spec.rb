# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User creates Page', type: :feature do
  before do
    use_authorized_account
  end

  scenario 'there is a markdown editor field' do
    visit root_path

    click_on 'Seite erstellen'

    expect(page).to have_css '.markdown-editor'
  end
  scenario 'successfully' do
    visit root_path

    click_on 'Seite erstellen'

    fill_in 'Titel', with: 'Philosophie'
    fill_in 'Langtitel', with: 'Die kath.de-Philosophie'
    fill_in 'Text', with: 'kath.de steht für Werte'

    click_on 'Speichern'

    expect(page).to have_content 'Die kath.de-Philosophie'
    expect(page).to have_content 'Entwurf'
  end
  # rubocop:enable RSpec/ExampleLength

  scenario 'unsuccessfully' do
    visit root_path

    click_on 'Seite erstellen'

    fill_in 'Titel', with: 'Die kath.de-Philosophie'
    fill_in 'Slug', with: '$pecial €haracters'

    click_on 'Speichern'

    expect(page).to have_css '#error_explanation'
  end
end
