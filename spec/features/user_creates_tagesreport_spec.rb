# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User creates Tagesreport', type: :feature do
  before do
    account = create(:account)
    use_authorized_account(account)
    create(:author, email: account.username)
    create(:author, display_name: 'Eckhard Bieger')
    Tagesreport.destroy_all
    Publisher.destroy_all
    create(:publisher, domain: 'de.radiovaticana.va', title: 'Radio Vatikan')
  end

  scenario 'there is a markdown editor field for aufmacher_body' do
    visit root_path

    click_on 'Tagesreport erstellen'

    expect(page).to have_css '[data-editor=markdown-editor][name="tagesreport[aufmacher_body]"]'
  end

  scenario 'default values are filled in' do
    visit root_path

    click_on 'Tagesreport erstellen'

    expect(page).to have_field 'Datum', with: Time.zone.today
    expect(page.find('//textarea[@id=tagesreport_entries]')).to have_content '"Bistümer und Werke":'
  end

  # rubocop:disable RSpec/ExampleLength
  scenario 'successfully' do
    visit root_path

    click_on 'Tagesreport erstellen'

    fill_in 'Aufmachertitel', with: 'Zeichen für die Ökumene'
    fill_in 'Aufmachertext', with: 'Papst Franziskus ist nach Lund gereist. '\
      'Dort hat er zusammen mit lutherischen Geistlichen der Reformation gedacht '\
      'und hat damit auch ein Zeichen für die Ökumene gesetzt.'
    fill_in 'Einträge', with: <<-HEREDOC.strip_heredoc
      "Themen des Tages":
      - url: http://de.radiovaticana.va/news/2016/10/30/der_papst_und_die_reformation_%E2%80%9Ewir_m%C3%BCssen_gemeinsam_beten%E2%80%9C/1267631
        titel: "Der Papst und die Reformation: „Wir müssen gemeinsam beten“"
        untertitel:
        tags:
      - url: http://www.rp-online.de/kultur/kardinal-nennt-trennung-der-kirchen-schmerzlich-aid-1.6345559
        titel: "Jerusalem: Kardinal nennt Trennung der Kirchen 'schmerzlich'"
    HEREDOC

    expect(Publisher['rp-online.de']).not_to be_persisted

    click_on 'Speichern'

    expect(page).to have_content 'Entwurf'
    expect(page).to have_content 'Zeichen für die Ökumene'
    expect(Publisher['rp-online.de']).to be_persisted
    expect(Publisher['rp-online.de'].title).to match 'RP ONLINE'

    click_on 'Veröffentlichen'

    expect(page).to have_content 'veröffentlicht'
  end
  # rubocop:enable RSpec/ExampleLength

  scenario 'unsuccessfully' do
    visit root_path

    click_on 'Tagesreport erstellen'

    fill_in 'Datum', with: 'Foobar'

    click_on 'Speichern'

    expect(page).to have_css '#error_explanation'
    expect(page).to have_content 'Datum muss ausgefüllt werden'
  end

  scenario 'with metadata interaction' do
  end

  scenario 'with invalid yaml' do
    visit root_path

    click_on 'Tagesreport erstellen'

    fill_in 'Aufmachertitel', with: 'Foo'
    fill_in 'Aufmachertext', with: 'Bar'
    fill_in 'Einträge', with: <<-HEREDOC.strip_heredoc
      Themen des Tages:
      - titel: """Fooo"""
      HEREDOC

    click_on 'Speichern'

    expect(page).to have_content 'Entwurf'

    expect(page).to have_content 'YAML Syntax-Fehler'

    click_on 'Veröffentlichen'

    expect(page).to have_content 'YAML Syntax-Fehler'
  end

  scenario 'with missing YAML data' do
    visit root_path

    click_on 'Tagesreport erstellen'

    fill_in 'Aufmachertitel', with: 'Foo'
    fill_in 'Aufmachertext', with: 'Bar'
    fill_in 'Einträge', with: <<-HEREDOC.strip_heredoc
      Themen des Tages:
      - titel: Foo
      # nur URL!
      HEREDOC

    click_on 'Speichern'

    expect(page).to have_content 'Entwurf'

    #expect(page).to have_content 'Eintrag Foo in Themen des Tages fehlt url'

    click_on 'Veröffentlichen'

    expect(page).to have_content 'Entwurf'
    expect(page).to have_content %(The property '#/Themen des Tages/0' did not contain a required property of 'url')
  end
end
