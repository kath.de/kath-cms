# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User views Kommentare index', type: :feature do
  let(:philipp) { create(:author, slug: 'philipp-mueller', display_name: 'Philipp Müller') }
  let(:published_kommentar) do
    create(:kommentar, :published,
           title: 'PhilippsPublishedKommentar', slug: 'published', author: [philipp])
  end
  let(:philipps_unpublished_kommentar) do
    create(:kommentar,
           title: 'PhilippsUnublishedKommentar', slug: 'notslug', author: philipp)
  end
  let(:philipps_entry) { create(:entry, slug: 'no-kommentar', author: philipp) }
  let(:unpublished_kommentar) { create(:kommentar, :unpublished, slug: 'unpublished') }
  let(:philipps_kommentare) { [published_kommentar, philipps_unpublished_kommentar] }
  let(:fulltext_kommentar) do
    create(:kommentar, :published,
           body: 'Lorem ipsum Dolor laudato si', title: 'Ferula und Stola')
  end

  before do
    use_authorized_account
    create_list(:author, 3)
    published_kommentar
    philipps_unpublished_kommentar
    philipps_entry
    unpublished_kommentar
    philipps_kommentare
    fulltext_kommentar
  end

  scenario 'shows published kommentar but not unpublished' do
    visit root_path
    click_on 'Kommentare'

    fill_in 'Suchen', with: 'is:published author:philipp-mueller'

    click_on 'Suchen'

    expect(page).to have_content published_kommentar.title

    expect(page).not_to have_content philipps_unpublished_kommentar.title
  end
end
