# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User visits dashboard', type: :feature do
  before do
    use_authorized_account
  end

  let(:current_user) { create(:account) }

  scenario 'is dashboard' do
    visit root_path
    expect(page).to have_content 'Dashboard'
  end

  scenario 'shows recent changes', versioning: true do
    PaperTrail.request.whodunnit = current_user
    kommentar = create(:kommentar)
    kommentar.publish!

    visit root_path

    within('//.recent-changes') do
      expect(page).to have_content 'Letzte Änderungen'

      expect(page).to have_content "#{current_user.username} publish Kommentar #{kommentar.title}"
    end
  end
end
