# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User navigates through breadcrumbs', type: :feature do
  before do
    use_authorized_account
  end

  scenario 'from kommentare to dashboard' do
    visit '/admin/kommentare'

    expect(page).to have_content 'Dashboard ❯ Kommentare'

    within('//.breadcrumbs') do
      click_link 'Dashboard'
    end

    expect(page.current_path).to match('/')
  end

  let(:kommentar) { create(:kommentar) }
  let(:doc_page) { create(:page, :hidden) }

  scenario 'from single kommentar to kommentar index' do
    # visit "/admin/kommentare/#{kommentar.id}/edit"
    visit edit_admin_kommentar_path(kommentar)

    within('//.breadcrumbs') do
      expect(page).to have_content("Dashboard ❯ Kommentare ❯ #{kommentar.title}")
      click_link kommentar.title
    end

    within('//.breadcrumbs') do
      expect(page).to have_content("Dashboard ❯ Kommentare ❯ #{kommentar.title}")
      click_link 'Kommentare'
    end

    within('//.breadcrumbs') do
      expect(page).to have_content('Dashboard ❯ Kommentare')
      click_link 'Dashboard'
    end

    expect(page.current_path).to match('/')
  end

  scenario 'docs page' do
    visit doc_path(doc_page.slug)

    within('//.breadcrumbs') do
      expect(page).to have_content("Dashboard ❯ Docs ❯ #{doc_page.title}")
      expect(page).to have_link('Docs', href: docs_path)
    end
  end
end
