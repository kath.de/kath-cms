# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User edits Autor', type: :feature do
  before do
    use_authorized_account
    create(:author)
    create(:author, display_name: 'Eckhard Bieger')
  end

  scenario 'and existing slug does not change' do
    visit root_path

    click_on 'Autoren'

    click_on 'Eckhard Bieger'

    click_on 'Bearbeiten'

    old_slug = find_field('Slug')['value']

    fill_in 'Name', with: 'Theo Hipp'

    click_on 'Speichern'

    expect(page).to have_content 'Theo Hipp'
    expect(page).to have_content old_slug
  end
end
