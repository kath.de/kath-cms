# frozen_string_literal: true

require 'rails_helper'
require 'kathde/models/spec_helper'

RSpec.feature 'User creates Image', type: :feature do
  before do
    use_authorized_account
    create(:author)
    create(:author, display_name: 'Eckhard Bieger')
  end

  def visit_new_page
    visit '/admin/images/new'
  end

  # scenario 'there is a markdown editor field' do
  #   visit root_path

  #   click_on 'Bild hochladen'

  #   expect(page).to have_css '.markdown-editor[data-uid]'
  # end

  scenario 'minimal: file only' do
    visit_new_page

    attach_file('image_image', Kathde::Models::ActiveStorageHelpers.model_file_fixture_path('image.jpg'))
    fill_in 'Quelle', with: 'Peter Pan 2019'

    click_on 'Speichern'

    expect(page).to have_content 'Peter Pan 2019'
    expect(page).to have_content 'image.jpg'
  end

  scenario 'with description' do
    visit_new_page

    attach_file('image_image', Kathde::Models::ActiveStorageHelpers.model_file_fixture_path('image.jpg'))

    fill_in 'Bildbeschreibung', with: 'Gummibärenbande im Kölner Dom'
    fill_in 'Quelle', with: 'Peter Pan 2019'

    click_on 'Speichern'

    expect(page).to have_content 'Gummibärenbande'
    expect(page).to have_content 'Peter Pan 2019'
  end

  scenario 'missing Quelle' do
    visit_new_page

    attach_file('image_image', Kathde::Models::ActiveStorageHelpers.model_file_fixture_path('image.jpg'))
    click_on 'Speichern'

    expect(page).to have_content 'Quelle muss ausgefüllt werden'
    expect(page).to have_css '#error_explanation'
  end

  scenario 'missing file' do
    visit_new_page

    fill_in 'Quelle', with: 'Peter Pan 2019'
    click_on 'Speichern'

    expect(page).to have_content 'Image muss ausgefüllt werden'
    expect(page).to have_css '#error_explanation'
  end
end
