# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User changes Settings', type: :feature do
  before do
    use_authorized_account
  end

  let(:setting) { create(:settings, slug: 'foo-setting', settings: { foo: 'bar' }) }

  scenario 'successfully' do
    visit admin_settings_path(setting)

    click_on 'Bearbeiten'

    fill_in 'Einstellungen', with: 'foo: "Hallo Welt"'

    click_on 'Speichern'

    expect(page).to have_content 'foo: Hallo Welt'
  end

  scenario 'unsuccessfully' do
  end
end
