require 'rails_helper'

RSpec.feature 'User lists Kommentare', type: :feature do
  before do
    use_authorized_account
  end

  scenario 'ordering by a author' do
    visit root_path

    click_on 'Kommentare'
    click_on 'Autor'
  end

  scenario 'ordering by a payload field' do
    visit root_path


    click_on 'Kommentare'
    click_on 'Titel'

    within('//.collection-data') do
      #puts find(:xpath, './/tr/td').inspect
      puts page.inspect
      expect(page).to have_content("foooooo")
    end
  end
end
