# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User publishes Kommentar', type: :feature do
  before do
    use_authorized_account
  end

  context 'previously unpublished' do
    subject { create(:kommentar, :unpublished) }

    scenario 'will not published' do
      visit admin_kommentar_path(subject)
      click_on 'Veröffentlichen'

      expect(page).to have_content('wurde veröffentlicht')
      expect(page).not_to have_content('unveröffentlicht')
    end
  end

  context 'previously published' do
    subject { create(:kommentar, :published) }

    scenario 'will be unpublished' do
      visit admin_kommentar_path(subject)

      click_on 'Unveröffentlichen'

      expect(page).to have_content('wurde unveröffentlicht')
      expect(page).to have_content('unveröffentlicht')
    end
  end

  context 'that is invalid' do
    let(:kommentar) { create(:kommentar, :unpublished, slug: nil) }

    scenario 'will not be published' do
      visit admin_kommentar_path(kommentar)

      click_on 'Veröffentlichen'

      expect(page).to have_content('Slug muss ausgefüllt werden')
    end
  end
end
