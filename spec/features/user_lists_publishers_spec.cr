require 'rails_helper'

RSpec.feature 'User lists Quellen', type: :feature do
  before do
    use_authorized_account
  end

  scenario 'ordering by a domain' do
    visit root_path

    click_on 'Quellen'
    click_on 'Domain'
  end
end
