# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User reads aktualisierungshinweise', type: :feature do
  before do
    use_authorized_account
    anleitung
  end

  let(:anleitung) do
    create :page, :hidden, slug: 'intern/aktualisierung', title: 'Redaktionsanleitung', body:
      <<-MARKDOWN.strip_heredoc
      # A. Redaktionspostfach

      Redaktionspostfach bearbeiten

      <a href="https://www.kath.de/webmail" class="btn">Redaktionspostfach</a>
      MARKDOWN
  end

  scenario 'from dashboard' do
    visit root_path

    click_link 'Anleitungen'

    click_link anleitung.title

    expect(page).to have_xpath('//h2', text: 'A. Redaktionspostfach')

    expect(page).to have_link('Redaktionspostfach', href: 'https://www.kath.de/webmail')
  end
end
