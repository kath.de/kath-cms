# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User creates Autor', type: :feature do
  before do
    use_authorized_account
  end

  scenario 'successfully' do
    visit root_path

    click_on 'Autor erstellen'

    fill_in 'Name', with: 'Eckhard Bieger'
    fill_in 'E-Mail', with: 'bieger@kath.de'

    click_on 'Speichern'

    expect(page).to have_content 'Eckhard Bieger'
    expect(page).to have_content 'eckhard-bieger'
    expect(page).to have_content 'bieger@kath.de'
  end

  scenario 'unsuccessfully' do
    visit root_path

    click_on 'Autor erstellen'

    # fill_in 'Name', with: 'Gummibärenbande im Kölner Dom'
    fill_in 'E-Mail', with: 'doreludio'

    click_on 'Speichern'

    expect(page).to have_css '#error_explanation'
  end
end
