# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User creates Kommentar', type: :feature do
  before do
    use_authorized_account
    create(:author)
    create(:author, display_name: 'Eckhard Bieger')
  end

  def visit_new_page
    visit '/admin/kommentare/new'
  end

  scenario 'there is a markdown editor field' do
    visit root_path

    click_on 'Kommentar schreiben'

    expect(page).to have_css '.markdown-editor[data-uid]'
  end

  scenario 'successfully' do
    visit_new_page

    fill_in 'Titel', with: 'Gummibärenbande im Kölner Dom'
    fill_in 'Text',
            with: 'Auf Einladung von *Kardinal Woelki* war die Gummibärenbande im Kölner Dom.'

    select 'Eckhard Bieger', from: 'Autor'

    click_on 'Speichern'

    expect(page).to have_content 'Gummibärenbande'
    expect(page).to have_content 'Entwurf'
    expect(page).to have_content 'Eckhard Bieger'
  end

  scenario 'unsuccessfully' do
    visit_new_page

    fill_in 'Titel', with: 'Gummibärenbande im Kölner Dom'
    fill_in 'Slug', with: '$pecial €haracters'

    click_on 'Speichern'

    expect(page).to have_css '#error_explanation'
  end
end
