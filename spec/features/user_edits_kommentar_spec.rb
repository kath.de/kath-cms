# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'User edits Kommentar', type: :feature do
  before do
    use_authorized_account
  end

  let(:kommentar) { create(:kommentar) }

  scenario 'successfully' do
    visit edit_admin_kommentar_path(kommentar)

    fill_in 'Titel', with: 'Ein anderer Titel'

    click_on 'Speichern'

    expect(page).to have_content 'Ein anderer Titel'

    #expect(page).to have_content '1 Version'
  end
end
