# frozen_string_literal: true

require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the DocsHelper. For example:
#
# describe DocsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ApplicationHelper, type: :helper do
  describe 'markdown' do
    subject(:helpers) { Class.new }

    before do
      helpers.extend described_class
    end

    let(:markdown) do
      <<-MARKDOWN.strip_heredoc
      # A. Redaktionspostfach

      Redaktionspostfach bearbeiten

      <a href="https://www.kath.de/webmail" class="btn">Redaktionspostfach</a>
      MARKDOWN
    end

    let(:html) do
      <<-HTML.strip_heredoc
      <h2 id="a-redaktionspostfach">A. Redaktionspostfach</h2>

      <p>Redaktionspostfach bearbeiten</p>

      <p><a href="https://www.kath.de/webmail" class="btn">Redaktionspostfach</a></p>
      HTML
    end

    it 'transforms markdown to html' do
      expect(helpers.markdown(markdown)).to match html
    end
  end
end
