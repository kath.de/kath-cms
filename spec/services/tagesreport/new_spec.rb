# frozen_string_literal: true

require 'rails_helper'

describe Tagesreport::New, type: :model do
  describe 'empty date' do
    it 'published with current date by default' do
      expect(Tagesreport::New.call(Tagesreport).resource.date).to eq(Time.zone.today)
    end
  end
  describe 'entries' do
    let(:entries) do
      %("Themen des Tages":\n\n"Service und Spiritualität":\n\n) +
        %("Bistümer und Werke":\n\n"Pressespiegel":\n)
    end

    it 'published with current date by default' do
      expect(Tagesreport::New.call(Tagesreport).resource.entries).to eq(entries)
    end
  end
end
