# frozen_string_literal: true

require 'rails_helper'

describe Entry::Persist, type: :model do
  describe Author do
    describe 'image' do
      it 'attaches image' do
        entry = Entry::Persist.new(Image, image: create_file_blob).prepare.resource

        expect(entry.image.attached?).to be true
      end
    end
  end
end
