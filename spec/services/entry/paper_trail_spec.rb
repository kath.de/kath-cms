# frozen_string_literal: true

require 'rails_helper'
require 'spec_helper'

describe Entry, type: :model, versioning: true do
  let(:test_title) { 'Test Titel' }

  describe '#versions' do
    context 'new entry' do
      let(:entry) { Entry::Persist.new(Kommentar, title: test_title).call.resource }

      it 'has a version entry' do
        PaperTrail.request.whodunnit = create(:account)

        expect(entry.versions.size).to eq(1)
      end
    end

    context 'existing entry' do
      let(:entry) { create(:kommentar) }

      it 'adds a version entry' do
        PaperTrail.request.whodunnit = create(:account)

        original_title = entry.title
        original_updated_at = entry.updated_at

        Entry::Persist.new(entry, title: test_title).call
        expect(entry.versions.size).to eq(2)
        expect(entry.versions.first.entry_type).to eq("Kommentar")
        expect(entry.versions[1].changeset["title"]).to eq([original_title, test_title])
      end
    end
  end
end
