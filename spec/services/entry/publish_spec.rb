# frozen_string_literal: true

require 'rails_helper'

describe Entry::Publish, type: :model do
  it 'publishes an entry', versioning: true do
    PaperTrail.request.whodunnit = create(:account)
    
    entry = create(:kommentar, :unpublished)
    Entry::Publish.call(entry)

    expect(entry.published_at).to_not be_nil
    expect(entry.versions.last.changeset).to include({'status' => ['draft', 'published'], "published_at" => [nil, entry.published_at]})
    expect(entry.versions.last.event).to eq("publish")
  end
end
