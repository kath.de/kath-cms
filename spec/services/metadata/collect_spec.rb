# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a set of metadata' do |values|
  let(:service) { Metadata::Collect.new(url).call }
  let(:metadata) { service.metadata }

  if values.key? :published_at
    published_at = values.delete(:published_at)
    it "contains 'published_at' with about #{published_at}" do
      expect(metadata.published_at).to be_within(1.minute).of(published_at)
    end
  end

  values.each do |key, value|
    it "contains '#{key}' with '#{value}'" do
      expect(metadata.send(key)).to eq(value)
    end
  end
end

describe Metadata::Collect do
  let(:service) { described_class.new(url).call }
  let(:metadata) { service.metadata }

  describe 'for Radio Vatican website' do
    let(:url) { 'http://de.radiovaticana.va/news/2016/10/20/heiliges_land_%C3%B6kumenische_wallfahrt_macht_hoffnung/1266502' }

    it('identifies title') do
      expect(metadata.title).to eq('Heiliges Land: Ökumenische Wallfahrt macht Hoffnung')
    end

    it { expect(metadata.subtitle).to eq('Heiliges Land: Ökumenische Wallfahrt macht Hoffnung - Oft sagt man &amp;uuml;ber &amp;Ouml;kumene: An der Basis ist alles gekl&amp;auml;rt, eigentlich m&amp;uuml;ssen sich nur noch die Kirchen einigen.') }
    it { expect(metadata.published_at).to be_within(1.minute).of(Time.zone.parse('2016-10-20')) }
  end

  context 'The Guradian article' do
    let(:url) { 'https://www.theguardian.com/us-news/2016/oct/24/elizabeth-warren-hillary-clinton-nasty-women-donald-trump' }

    it { expect(metadata.title).to eq("Elizabeth Warren: 'nasty women' will defeat Donald Trump on election day") }
    it { expect(metadata.subtitle).to eq('Massachusetts senator and Trump tormenter-in-chief turns insult into rallying cry in support of Hillary Clinton, as partnership hints at progressive agenda') }
    it { expect(metadata.published_at).to be_within(1.minute).of(Time.zone.parse('2016-10-24 21:18 BST')) }
    it { expect(metadata.publisher).to match(/The Guardian/i) }
    # it { pending; expect(metadata.authors).to contain_exactly('Lauren Gambino', 'Dan Roberts') }
  end

  # context 'Bistum Limburg' do
  # let(:url) { 'https://www.bistumlimburg.de/meldungen/meldung-detail/meldung/eine-million-sterne-in-frankfurt.html' }
  # hat mal gar nix...

  # it { pending('crappy content'); expect(metadata.title).to eq('„Eine Million Sterne“ in Frankfurt') }
  # it { pending('crappy content'); expect(metadata.subtitle).to eq('Knips ein Licht an für junge Flüchtlinge in Frankfurt und Marokko') }
  # it { pending('has no metadata'); expect(metadata.published_at).to be_within(1.minute).of(Time.zone.parse('24.10.2016')) }
  # it { pending('needs publisher model'); expect(metadata.publisher).to eq('Bistum Limburg') }
  # end

  context 'Erzistum Bamberg' do
    let(:url) { 'http://erzbistum-bamberg.de/nachrichten/zukunftsmusik-beim-neuen-geistlichen-lied/418ff0f2-d20b-463b-943d-60866f976829?mode=detail' }

    it { expect(metadata.title).to eq('Zukunftsmusik beim Neuen Geistlichen Lied') }
    # it { pending('has no metadata'); expect(metadata.subtitle).to eq('Neugründung des ersten NGL-Verbandes erfolgte im Rahmen einer Versammlung') }
    # it { pending('has no metadata'); expect(metadata.published_at).to be_within(1.minute).of(Time.zone.parse('24.10.2016')) }
    # it { pending('needs publisher model'); expect(metadata.publisher).to eq('Erzbistum Bamberg') }
  end
end
