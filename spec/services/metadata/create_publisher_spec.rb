# frozen_string_literal: true

require 'rails_helper'

describe Metadata::CreatePublisher do
  let(:service) { described_class.call(metadata) }

  class Testdata
    attr_accessor :url, :publisher

    def initialize(hash)
      hash.each do |key, value|
        send("#{key}=", value)
      end
    end
  end

  context 'new provider' do
    let(:metadata) do
      Testdata.new(
        url: 'https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd',
        publisher: 'Domradio',
      )
    end

    it('returns persisted publisher') do
      expect(service.resource).to be_persisted
    end

    it('creates a new publisher') do
      expect { service.resource }.to change { Publisher.count }.by(1)
    end

    describe '#domain' do
      it('is correctly derived') { expect(service.resource.domain).to eq('domradio.de') }
    end

    describe '#title' do
      let(:metadata) do
        Testdata.new(
          url: 'https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd',
          publisher: 'Domradio',
        )
      end

      context 'publisher explicitly provided' do
        it('is correctly set') { expect(service.resource.title).to eq('Domradio') }
      end

      context 'no publisher provided' do
        let(:metadata) do
          Testdata.new(
            url: 'https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd',
            publisher: nil,
          )
        end

        it('is correctly derived') { expect(service.resource.title).to eq('Domradio.de') }
      end
    end
  end

  context 'existing provider' do
    let(:metadata) do
      Testdata.new(
        url: 'https://www.domradio.de/themen/%C3%B6kumene/2016-10-24/kirchentagspraesidentin-zum-reformator-und-umgang-mit-der-afd',
        publisher: 'Domradio',
      )
    end

    let!(:domradio_entry) { create(:publisher, domain: 'domradio.de', title: 'Domradio Original') }

    it('returns persisted publisher') do
      expect(service.resource).to eq(domradio_entry)
    end

    it('does not create a new publisher') do
      expect { service.resource }.not_to change { Publisher.count }
    end
  end
end
