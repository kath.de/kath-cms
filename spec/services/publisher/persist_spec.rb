# frozen_string_literal: true

require 'rails_helper'

describe Publisher::Persist, type: :model do
  describe '#reduce_url_to_domain' do
    let(:service) { Publisher::Persist.new(Publisher, domain: 'http://www.kath.de/redaktion').prepare }

    it 'transforms url string' do
      expect(service.resource.domain).to eq('kath.de')
    end
  end

  describe '#normalize_domain' do
    let(:service) { Publisher::Persist.new(Publisher, domain: 'Kath.De').prepare }

    it 'transforms url string' do
      expect(service.resource.domain).to eq('kath.de')
    end
  end

  describe '#generate_slug' do
    let(:service) { Publisher::Persist.new(Publisher, domain: 'http://www.Kath.de/redaktion').prepare }

    it { expect(service.resource.slug).to eq('kath.de') }
  end

  describe '#generate_title' do
    let(:service) { Publisher::Persist.new(Publisher, domain: 'http://www.Kath.de/redaktion').prepare }

    it { expect(service.resource.title).to eq('Kath.de') }
  end

  describe '#call' do
    let(:service) do
      Publisher::Persist.new(Publisher, domain: 'http://www.Kath.de/redaktion',
                                        title: 'Kath.De Titel',
                                        long_title: 'Kath.de gGmbH')
    end

    let(:publisher) { service.call.resource }

    it 'is success' do
      expect(service.call).to be_success
    end

    it 'saves valid publisher' do
      expect { service.call }.to change { Publisher.count }.by(1)
    end

    describe 'assigns correct values' do
      it('title') { expect(publisher.title).to eq('Kath.De Titel') }
      it('long_title') { expect(publisher.long_title).to eq('Kath.de gGmbH') }
      it('domain') { expect(publisher.domain).to eq('kath.de') }
      it('slug') { expect(publisher.slug).to eq('kath.de') }
    end
  end
end
