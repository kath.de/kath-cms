# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Entry, type: :model do
  describe '#payload_attributes' do
    it 'contains all attributes' do
      expect(PayloadEntry.new.payload_attributes).to match(title: :string, active: :boolean)
    end
  end
  describe '#payload_attribute_names' do
    it 'contains all attribute names' do
      expect(PayloadEntry.new.payload_attribute_names).to match(%i[title active])
    end
  end
end

class PayloadEntry < Entry
  PAYLOAD_ATTRIBUTES = {
    title: :string,
    active: :boolean,
  }.freeze

  jsonb_accessor(:payload, PAYLOAD_ATTRIBUTES)
end
