# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Search::Terms, type: :model do
  describe '.parse' do
    let(:complex_term) { 'author:maximilian-roell author:philipp-mueller is:kommentar is:published Freibier Franziskus title:"Habemus papam"' }
    let(:terms) { Search::Terms.parse(complex_term) }

    it('parses any fields') do
      expect(terms.any).to contain_exactly 'Freibier', 'Franziskus'
    end

    it('parses author fields') do
      expect(terms[:author]).to contain_exactly 'philipp-mueller', 'maximilian-roell'
    end

    it('parses scopes') do
      expect(terms.scopes).to contain_exactly :published, :kommentar
    end

    it('parses wildcard') do
      expect(Search::Terms.parse('author:*')[:author]).to contain_exactly '*'
    end
  end
end
