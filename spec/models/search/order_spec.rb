# frozen_string_literal: true

# rubocop:disable RSpec/MessageChain
require 'rails_helper'

describe Search::Order, type: :model do
  describe '#apply' do
    context 'when `order` argument is nil' do
      it "doesn't sort the resources" do
        order = Search::Order.new(nil, :desc)
        relation = relation_with_column(:id)
        allow(relation).to receive(:order).and_return(relation)

        ordered = order.apply(relation)

        expect(relation).not_to have_received(:order)
        expect(ordered).to eq(relation)
      end
    end

    context "when `order` argument isn't a valid column" do
      it 'ignores the order' do
        order = Search::Order.new(:foo)
        relation = relation_with_column(:id)
        allow(relation).to receive(:order).and_return(relation)
        allow(relation).to receive_message_chain(:model, :arel_column_node).and_return(nil)

        ordered = order.apply(relation)

        expect(relation).not_to have_received(:order)
        expect(ordered).to eq(relation)
      end
    end

    context 'when `order` argument is valid' do
      it 'orders by the column' do
        order = Search::Order.new(:slug, :asc)
        relation = relation_with_column(:slug)
        allow(relation).to receive(:order).and_return(relation)
        allow(relation).to receive_message_chain(:model, :arel_column_node).and_return(:slug)

        ordered = order.apply(relation)

        expect(relation).to have_received(:order).with(slug: :asc)
        expect(ordered).to eq(relation)
      end

      it 'honors the `direction` argument' do
        order = Search::Order.new(:slug, :desc)
        relation = relation_with_column(:slug)
        allow(relation).to receive(:order).and_return(relation)
        allow(relation).to receive_message_chain(:model, :arel_column_node).and_return(:slug)

        ordered = order.apply(relation)

        expect(relation).to have_received(:order).with(slug: :desc)
        expect(ordered).to eq(relation)
      end
    end
  end

  describe '#ordered_by?' do
    it 'returns true if the order is by the given attribute' do
      order = Search::Order.new(:slug, :desc)

      expect(order).to be_ordered_by('slug')
    end

    it 'returns false if the order is not by the given attribute' do
      order = Search::Order.new(:email, :desc)

      expect(order).not_to be_ordered_by(:slug)
    end

    it 'returns false if there is no order' do
      order = Search::Order.new(nil, :desc)

      expect(order).not_to be_ordered_by(:slug)
    end
  end

  describe '#order_params_for' do
    context 'when there is no order' do
      it 'returns the attribute' do
        order = Search::Order.new(nil)

        params = order.order_params_for(:slug)

        expect(params[:order]).to eq(:slug)
      end

      it 'does not sort descending' do
        order = Search::Order.new(nil)

        params = order.order_params_for(:slug)

        expect(params[:direction]).to eq(:asc)
      end
    end

    context 'when the order is by a different attribute' do
      it 'returns the attribute' do
        order = Search::Order.new(:email)

        params = order.order_params_for(:slug)

        expect(params[:order]).to eq(:slug)
      end

      it 'sorts ascending' do
        order = Search::Order.new(:email)

        params = order.order_params_for(:slug)

        expect(params[:direction]).to eq(:asc)
      end
    end

    context 'when the data is already ordered by the given attribute' do
      it 'returns the attribute' do
        order = Search::Order.new(:slug)

        params = order.order_params_for(:slug)

        expect(params[:order]).to eq(:slug)
      end

      it "orders the data descending if it's not already" do
        order = Search::Order.new('slug')

        params = order.order_params_for(:slug)

        expect(params[:direction]).to eq(:desc)
      end

      it 'sets direction ascending if the data is already descending' do
        order = Search::Order.new(:slug, 'desc')

        params = order.order_params_for(:slug)

        expect(params[:direction]).to eq(:asc)
      end
    end
  end

  def relation_with_column(column)
    double(columns_hash: { column.to_s => :column_info }) # rubocop:disable RSpec/VerifiedDoubles
  end
end
