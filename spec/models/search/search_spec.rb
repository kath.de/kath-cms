# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Search::Search, type: :model do
  let!(:philipp) { create(:author, slug: 'philipp-mueller', display_name: 'Philipp Müller') }
  let!(:published_kommentar) { create(:kommentar, :published, slug: 'published', author: [philipp]) }
  let!(:philipps_unpublished_kommentar) { create(:kommentar, slug: 'notslug', author: philipp) }
  let!(:philipps_entry) { create(:entry, slug: 'no-kommentar', author: philipp) }
  let!(:unpublished_kommentar) { create(:kommentar, :unpublished, slug: 'unpublished') }
  let!(:philipps_kommentare) { [published_kommentar, philipps_unpublished_kommentar] }
  let!(:fulltext_kommentar) { create(:kommentar, :published, body: 'Lorem ipsum Dolor laudato si', title: 'Ferula und Stola') }
  let!(:unpublished_kommentar_without_author) { create(:kommentar, :unpublished, author: nil) }

  before do
    create_list(:author, 3)
  end

  describe '#results' do
    def search(terms)
      Search::Search.new(Kommentar, terms).results
    end

    it 'finds all on empty search' do
      expect(search('')).to eq(Kommentar.all)
    end

    it 'finds kommentar based on author' do
      expect(search('author:philipp-mueller')).to contain_exactly(*philipps_kommentare)
    end

    it 'finds kommentar based on author and status' do
      expect(search('author:philipp-mueller is:published')).to contain_exactly(published_kommentar)
    end

    it 'finds kommentar based on title' do
      expect(search('Ferula')).to contain_exactly(fulltext_kommentar)
    end

    it 'finds kommentar based on fulltext search' do
      expect(search('"laudato si" STOLA')).to contain_exactly(fulltext_kommentar)
    end

    it 'is empty when contradicted' do
      expect(search('is:published is:unpublished')).to be_empty
    end

    it 'ignores unknown fields' do
      expect(search('foo:bar author:philipp-mueller')).to contain_exactly(*philipps_kommentare)
    end

    it 'accepts wildcard' do
      expect(search('author:*')).to contain_exactly(*philipps_kommentare, fulltext_kommentar, unpublished_kommentar)
    end
  end
end
