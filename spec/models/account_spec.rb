# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Account, type: :model do
  describe '.from_cas' do
    context 'when account is authorized' do
      let(:authorized_account) { create(:account) }
      let(:cas_authenticated_account) do
        described_class.from_cas('user' => authorized_account.username)
      end

      it 'loads account' do
        expect(cas_authenticated_account).to match(authorized_account)
      end

      it 'is authorized' do
        expect(cas_authenticated_account).to be_authorized
      end
    end

    context 'when account is unknown' do
      let(:unregisterd_username) { 'unknown@example.com' }
      let(:cas_authenticated_account) { described_class.from_cas('user' => unregisterd_username) }

      it 'loads unknown account' do
        expect(cas_authenticated_account.username).to match(unregisterd_username)
      end

      it 'is not authorized' do
        expect(cas_authenticated_account).not_to be_authorized
      end
    end
  end
end
