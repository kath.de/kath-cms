# frozen_string_literal: true

FactoryBot.define do
  factory :settings do
    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }

    slug { 'foo-settings' }
    settings { { foo: 'bar' } }
  end
end
