# frozen_string_literal: true

FactoryBot.define do
  factory :author do
    sequence(:display_name) do |n|
      f = %w[Hans Peter Klaus Thomas Jochen Michael Matthias Peter Paul Stefan Kilian]
      l = %w[Müller Maier Schmidt Huber Bauer Henkel Rose Wagner Schneider Hirsch]
      "#{f[n % f.size]} #{l[n % l.size]}"
    end

    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }

    slug do
      display_name.to_s.parameterize
    end

    initials do
      display_name.split(/ /).map { |s| s[0] }.join.downcase
    end

    email do
      "#{slug}@kath.de"
    end
  end
end
