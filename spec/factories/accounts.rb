# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    sequence(:username) do |n|
      suffix = n == 0 ? 'n' : "-#{n}"
      "tester#{suffix}@test.kath.de"
    end
  end
end
