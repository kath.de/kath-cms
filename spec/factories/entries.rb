# frozen_string_literal: true

FactoryBot.define do
  factory :entry do
    sequence(:slug) { |n| "example-entry-#{n}" }

    type { 'Entry' }
    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }

    trait :unpublished do
      published_at { nil }
      status { 'draft' }
    end

    trait :published do
      published_at { '2016-10-08 12:50:34' }
      status { 'published' }
    end
  end
end
