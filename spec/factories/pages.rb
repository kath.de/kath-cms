# frozen_string_literal: true

FactoryBot.define do
  factory :page do
    sequence(:title) do |n|
      a = ['Gummibärenbande im Kölner Dom',
           'Gänseblümchen für Gänswein: Papst Benedikt gratuliert im Vatikanischen Garten',
           'Habemus Papam: Halbwaisen freuen sich über Hochzeit ihrer Mutter',
           'Hello World',
           'Lorem ipsum',
           'Hokus Pokus',
           'Railing on Rubies',]
      a[n % a.size]
    end

    body { 'Auf Einladung von Kardinal Woelki hüpfte die Gummibärenbande durch den Kölner Dom.' }
    created_at { '2016-10-08 12:50:34' }
    updated_at { '2016-10-08 12:50:34' }

    slug do
      title.parameterize
    end

    trait :unpublished do
      published_at { nil }
    end

    association :author, factory: :author

    trait :published do
      published_at { '2016-10-08 12:50:34' }
      status { 'published' }
    end

    trait :hidden do
      status { 'hidden' }
    end
  end
end
