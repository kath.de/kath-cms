# frozen_string_literal: true

source 'https://rubygems.org'
ruby '~> 2.7.0'

# Bundle edge Rails instead: gem 'rails', git: 'https://github.com/rails/rails.git'
gem 'rails', '~> 6.0'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '~> 4.2.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.8'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

gem 'administrate'
# gem 'administrate', path: '../administrate'
gem 'administrate-field-active_storage'

gem 'kathde-models', git: 'https://gitlab.com/kath.de/kathde-models.git'
# gem 'kathde-models', path: '../kathde-models'
gem 'paper_trail', '~> 11.0'
# gem 'postgres_ext'

gem 'kramdown'

gem 'activerecord_json_validator'
gem 'metainspector', git: 'https://github.com/straight-shoota/metainspector.git'

gem 'thumbor_rails'
gem "aws-sdk-s3", require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails' #, '~> 3.5'
  # pretty print Ruby objects to visualize their structure.
  gem 'awesome_print'
  gem 'bundler-audit'
  # gem 'better_errors'
  # gem 'binding_of_caller'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # static code analyzer for https://github.com/bbatsov/ruby-style-guide
  gem 'rubocop', require: false
  gem 'rubocop-rspec', require: false
  # detects security vulnerabilities in Ruby on Rails applications via static analysis.
  gem 'brakeman', require: false

  gem 'rails_best_practices', require: false

  gem 'annotate', require: false
  # Use pry as rails console
  gem 'pry-rails'

  # TODO: replace fakeweb with webmock
  gem 'fakeweb-fi'

  gem 'image_processing'
end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'poltergeist'
  gem 'rack_session_access'
  gem 'simplecov', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'rack-cas', git: 'https://github.com/biola/rack-cas.git'
gem 'rails-i18n'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'kath-style', git: 'https://gitlab.com/kath.de/kath-style.git'
