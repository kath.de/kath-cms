# Bugs
* Anzeige "veröffentlicht" stimmt nicht ✓
* Datumswidget erzeugt fehlerhaftes Datum: Replace custom datetime_picker widget ✓
* Fehler bei Metadata-Call. Verbindungsprobleme? Provider-Eintrag auch bei create/update erzeugen, nicht nur durch Metadata-Call
* Tagesreport:
  - Fehler bei Kommentar-Speichern: Slug fehlt

# Critical
* Tagesreport ✓
    - Content type & controller ✓
    - Client-side JS ✓
    - Auto-Insert Datum ✓
* Wochenrückblick (testen)
* Autor
    - Auto-Insert Autor ✓
    - Autorenseite (Text + Verlinkung der Texte)
    - Verlinkung auf Texte etc.
* User login ✓
* File upload
* Ensure publisher records are created on tagesreport update
* collection search is broken ✓
* Soft delete
* Preview in final template (Frontend)
* Validation
    - YAML
    - JSON Schema (unvollständig)
    - URLs (testen)
    - E-Mail (testen)
* Editor help base
    - Markdown
    - Aktualisierungsseite
* Menü
    - Aktualisierungsseite
    - Redaktions-Postfach ✓
* Dev-Documentation
* Konfiguration
    - Werbebanner
    - Hauptseiten-Links
    - Aktualisierungsseite
* Dashboardausstattung
* Test services!

# Desired
* content versioning
* event messaging
* metadata insert error messages
* background processing for metadata insert editor
* breadcrumbs

# Potential
* Spell checker for Markdown fields (cf https://github.com/NextStepWebs/codemirror-spell-checker/issues/16)
* Replace codemirror with ACE:
    - better YAML string wrap
