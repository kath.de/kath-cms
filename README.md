# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

## Database creation
`````
DROP ROLE IF EXISTS #{dbconfig['development']['username']};
CREATE USER #{dbconfig['development']['username']} WITH 
PASSWORD '#{dbconfig['development']['password']}' CREATEDB CREATEROLE;
`````
`sudo -u postgres psql -c "#{sql}"`

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

## Test suite
Run `rspec` to run all tests

`COVERAGE=true rspec` also checks code coverage with simplecov. Output in `coverage/index.html`
